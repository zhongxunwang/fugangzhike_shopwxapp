const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_index_data: null,
    },
    onLoad: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/CmsApi/index', requestData, function (cyz_data) {
            console.log(cyz_data)
            wx.hideToast();
            that.setData({
                this_index_data: cyz_data.info
            });
            WxParse.wxParse('article', 'html', cyz_data.info.index_page_about.page_content, that, 5);
        });
    }
})