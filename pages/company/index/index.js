const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_index_data:null,
        this_cms_config:[],
        index_ad_1:null,
        index_ad_2:null,
        btn_submit_disabled: false,
        submitIsLoading: false,
        shop_ruzhu_list:null,
        shop_goods_list:null
    },
    onLoad:function(){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/CmsApi/index', requestData, function (cyz_data) {
            console.log(cyz_data)
            wx.hideToast();
            that.setData({
                this_index_data: cyz_data.info,
                this_cms_config: cyz_data.info.index_config,
                shop_ruzhu_list: cyz_data.info.shop_ruzhu_list,
                shop_goods_list: cyz_data.info.shop_goods_list
                });
            wx.setNavigationBarTitle({
                title: cyz_data.info.index_config.cms_title
            });
            that.get_shop_ad();
        });
    },
    get_shop_ad:function(){
        var that = this;
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopAd/index', requestData, function (cyz_data) {
            console.log(cyz_data)
            that.setData({
                index_ad_1: cyz_data.info.index_ad_1,
                index_ad_2: cyz_data.info.index_ad_2,
            });
        });
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const  mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //广告链接跳转
    go_ad_url_bind:function(e){
        wx.navigateTo({url: e.currentTarget.dataset.url});
    },
    //链接跳转
    go_url_bind:function(e){
        wx.navigateTo({url: e.currentTarget.dataset.url});
    },
    go_piclist_info:function(e){
        var tid = e.currentTarget.id;
        wx.navigateTo({ url: "../single/index?id="+tid});
    },
    go_newslist_info:function(e){
        var tid = e.currentTarget.id;
        wx.navigateTo({ url: "../single-news/index?id=" + tid });
    },
    //地图跳转
    go_map_info_b: function () {
        var that = this;
        var t_address = that.data.this_cms_config.cms_address;
        _requsetCYZ.cyz_requestGet('/Map/getLocation', { address: t_address }, function (xyz_data) {
            wx.openLocation({
                latitude: parseFloat(xyz_data.info.lat),
                longitude: parseFloat(xyz_data.info.lng),
                scale: 18,
                address: t_address
            });
        });
    },
    //提交留言
    message_formSubmit:function(e){
        var that = this;
        that.setData({ btn_submit_disabled: true, submitIsLoading: true });
        _functionCYZ.CYZ_loading();
        var requestData = e.detail.value;
        _requsetCYZ.cyz_requestGet('/CmsApi/addMessage', requestData, function (cyz_data) {
            that.setData({ btn_submit_disabled: false, submitIsLoading: false });
            wx.hideToast();
            if (cyz_data.code == 1) {
                _functionCYZ.CYZ_alert(cyz_data.info, "/pages/company/index/index", "tab");
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.message_formSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.onLoad();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_cms_config.cms_title;
        var sharePath = 'pages/company/index/index';
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})