const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_index_data: null,
        this_cms_config:null
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //地图跳转
    go_map_info_b: function () {
        var that = this;
        var t_address = that.data.this_cms_config.cms_address;
        _requsetCYZ.cyz_requestGet('/Map/getLocation', { address: t_address }, function (xyz_data) {
            wx.openLocation({
                latitude: parseFloat(xyz_data.info.lat),
                longitude: parseFloat(xyz_data.info.lng),
                scale: 18,
                address: t_address
            });
        });
    },
    onLoad: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/CmsApi/index', requestData, function (cyz_data) {
            console.log(cyz_data)
            wx.hideToast();
            that.setData({
                this_index_data: cyz_data.info,
                this_cms_config: cyz_data.info.index_config
            });
            WxParse.wxParse('article', 'html', cyz_data.info.index_page_contact.page_content, that, 5);
        });
    }
})