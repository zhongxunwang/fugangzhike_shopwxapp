const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_data_info: null,
    },
    onLoad: function (op) {
        var that = this;
        let tid = op.id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.id = tid;
        _requsetCYZ.cyz_requestGet('/CmsApi/getArticleInfo', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 1) {
                that.setData({ this_data_info: xyz_data.info});
                wx.setNavigationBarTitle({
                    title: xyz_data.info.title
                });
                WxParse.wxParse('article', 'html', xyz_data.info.content, that, 5);
            } else {
                wx.navigateBack();
                return false;
            }
        });
    },
    go_single_bind: function (e) {
        let tid = e.currentTarget.id;
        wx.navigateTo({ url: "../single/index?id=" + tid });
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_data_info.title;
        var sharePath = 'pages/company/single-news/index?id=' + that.data.this_data_info.id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    },
})