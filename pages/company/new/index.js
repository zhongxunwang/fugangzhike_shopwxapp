const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        this_index_data: null,
    },
    onLoad: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.cate_id = 2;
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/CmsApi/getArticleList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({ this_index_data: cyz_data.info });
        });
    },
    go_single_bind: function (e) {
        let tid = e.currentTarget.id;
        wx.navigateTo({ url: "../single-news/index?id=" + tid });
    }
})