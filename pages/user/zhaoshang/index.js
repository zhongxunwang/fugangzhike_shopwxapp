const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        submitIsLoading: false,
        buttonIsDisabled: false,
        province:'',
        city:'',
        district:'',
        region: []
    },
    bindRegionChange: function (e) {
        let v = e.detail.value
        this.setData({
            province:v[0],
            city:v[1],
            district:v[2],
            region: v
        })
    },
    add_formSubmit: function (e) {
        var that = this;
        that.setData({submitIsLoading: true, buttonIsDisabled: true });
        _functionCYZ.CYZ_loading_n("提交中，请稍候...");
        var requestData = e.detail.value;
        requestData.province = that.data.province
        requestData.city = that.data.city
        requestData.district = that.data.district
        _requsetCYZ.cyz_requestPost('/UserRenzheng/userZixunV2', requestData, function (xyz_data) {
            wx.hideLoading();
            app.pushFormIdSubmit(e);
            if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert(xyz_data.info, "/pages/user/index", 'tab');
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.add_formSubmit(e);
                });
                return false;
            } else {
                that.setData({buttonIsDisabled: false, submitIsLoading: false });
                app.commonErrorTips(xyz_data.info);
                return false;
            }
        });
    }
});
