const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        is_show_now: false,
        this_data_now: null,
        this_data_list: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        btn_submit_disabled: false,
        submitIsLoading: false,
        this_ahead_fanxian_id: 0,
        is_show_fanxian_confirm: false,
        this_ahead_fanxian_info: null,
        is_show_jiasu_model:false,
        this_type: 0,
        is_show_tips: false,
        this_shop_config: null,
        this_fanxian_name: '',
        fanxian_tips_text: []
    },
    show_now_order_fanli_list: function () {
        this.setData({
            is_show_now: this.data.is_show_now ? false : true
        });
    },
    close_tips: function () {
        let _this = this;
        _functionCYZ.setStorageSyncTime('is_show_add_tips', 1, 3600 * 24 * 7);
        _this.setData({ is_show_tips: false });
    },
    // tab栏切换
    changeUserType: function (e) {
        let that = this;
        let this_type = e.currentTarget.dataset.type;
        that.setData({ this_type: this_type,this_page_size: 1, is_loadmore: true });
        wx.pageScrollTo({scrollTop: 0});
        that.getIndexData();
    },
    showJiasuModel:function(){
        this.setData({is_show_jiasu_model:this.data.is_show_jiasu_model?false:true});
    },
    onLoad: function () {
        var that = this;
        let is_show_add_tips = _functionCYZ.getStorageSyncTime('is_show_add_tips', '');
        if (is_show_add_tips) {
            that.setData({ is_show_tips: false });
        } else {
            that.setData({ is_show_tips: true });
        }
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        requestData.select_type = that.data.this_type;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserFanxianList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info.list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.list.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({
                this_data_list: xyz_data.info.list,
                this_data_now: xyz_data.info.fanxian_now,
                this_shop_config: xyz_data.info.shop_config_info,
                fanxian_tips_text: xyz_data.info.fanxian_tips_text
            });
            if (xyz_data.info.shop_config_info) {
                that.setData({ this_fanxian_name: xyz_data.info.shop_config_info.global_fanxian_name || '返现' });
                wx.setNavigationBarTitle({
                    title: xyz_data.info.shop_config_info ? '我的' + xyz_data.info.shop_config_info.global_fanxian_name : '我的订单'
                });
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.select_type = that.data.this_type;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserFanxianList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info.list == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.list.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_data_list;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info.list);
                    that.setData({ this_data_list: this_new_info_data, this_page_size: requestData.pagesize, this_data_now: xyz_data.info.fanxian_now });
                }
            }
        });
    },
    show_order_fanli_list: function (e) {
        var that = this;
        let this_key = e.currentTarget.dataset.key;
        let this_list_data = that.data.this_data_list;
        this_list_data[this_key].o_fanli_isshow = this_list_data[this_key].o_fanli_isshow ? false : true;
        that.setData({ this_data_list: this_list_data });
    },
    ahead_order_fanxian: function (e) {
        var that = this;
        var order_id = e.currentTarget.dataset.id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_id = order_id;
        _requsetCYZ.cyz_requestGet('/ShopShuju/checkFanxianOut', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.ahead_order_fanxian(e);
                });
                return false;
            } else if (xyz_data.code == 1) {
                that.setData({
                    this_ahead_fanxian_id: order_id,
                    this_ahead_fanxian_info: xyz_data.info,
                    is_show_fanxian_confirm: true
                });
            } else {
                app.commonErrorTips(xyz_data.info);
            }
        });
    },
    close_fanxian_confirm: function () {
        this.setData({
            this_ahead_fanxian_id: 0,
            this_ahead_fanxian_info: null,
            is_show_fanxian_confirm: false
        });
    },
    confrim_ahead_fanxian: function () {
        var that = this;
        var order_id = that.data.this_ahead_fanxian_id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_id = order_id;
        _requsetCYZ.cyz_requestGet('/ShopShuju/confrimFanxianOut', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.confrim_ahead_fanxian();
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.showModal({
                    title: '提示',
                    showCancel: false,
                    content: xyz_data.info,
                    success: function (res) {
                        that.close_fanxian_confirm();
                        that.setData({ this_page_size: 1, is_loadmore: true });
                        that.getIndexData();
                    }
                });
            } else {
                app.commonErrorTips(xyz_data.info);
            }
        });
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({ this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})