const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_shop_info_c:null,
        this_user_data: null,
        this_user_info:{}
    },
    onShow: function () {
        this.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/AccountApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_data: cyz_data.info,
                    this_shop_info_c: cyz_data.info.shop_info_config,
                    this_user_info:cyz_data.info.user_info
                });
                if(cyz_data.info.user_info.user_phone_status == 0){
                    wx.navigateTo({
                      url: '/pages/user/phone_binding/index'
                    })
                }
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }

        });
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
})