const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_shop_info_c: null,
        bank_list:null,
        picker_index: 0,
        bank_code:'',
        bank_name:'',
        this_cash_type:0,
        this_user_data: null,
        this_fendian_id:0,
        this_fendian_info:null,
        this_u_lng: 0,
        this_u_lat: 0,
        this_pay_img:'',
        btn_submit_disabled: false,
        submitIsLoading: false
    },
    onLoad: function () {
        if (wx.getStorageSync('order_fendian_id') > 0){
            this.setData({ this_fendian_id: wx.getStorageSync('order_fendian_id')});
        }
        this.get_user_location();
    },
    select_account_type:function(e){
        this.setData({ this_cash_type:e.currentTarget.id});
        wx.setStorageSync('this_cash_type', e.currentTarget.id);
    },
    get_user_location: function () {
        let that = this;
        if (wx.getStorageSync("order_user_lat") && wx.getStorageSync("order_user_lng")){
            that.setData({ this_u_lng: wx.getStorageSync("order_user_lng"), this_u_lat: wx.getStorageSync("order_user_lat") });
            that.get_index_data();return false;
        }
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                wx.setStorageSync('order_user_lat', res.latitude);
                wx.setStorageSync('order_user_lng', res.longitude);
                that.setData({ this_u_lng: res.longitude, this_u_lat: res.latitude });
                that.get_index_data();
            },
            fail: function () {
                that.get_index_data();
            }
        })
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        requestData.fendian_id = that.data.this_fendian_id||0;
        _requsetCYZ.cyz_requestGet('/AccountApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_data: cyz_data.info,
                    this_shop_info_c: cyz_data.info.shop_info_config,
                    bank_list: cyz_data.info.this_bank_list,
                    this_fendian_info: cyz_data.info.shop_fendian_info
                });
                that.setData({ this_cash_type: cyz_data.info.shop_info_config.cash_default_type });
                // if (that.data.this_shop_info_c.cash_is_weixin == 1 && that.data.this_shop_info_c.cash_is_daodian==1){
                //     if(wx.getStorageSync("this_cash_type")){
                //         that.setData({ this_cash_type:wx.getStorageSync("this_cash_type")});
                //     }
                // } 
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }

        });
    },
    cash_formSubmit: function (e) {
        var that = this;
        that.setData({ btn_submit_disabled: true, submitIsLoading: true });
        _functionCYZ.CYZ_loading();
        var requestData = e.detail.value;
        requestData.cash_type = that.data.this_cash_type;
        requestData.fendian_id = that.data.this_fendian_info.id || 0;
        requestData.pay_img = that.data.this_pay_img;
        _requsetCYZ.cyz_requestPost('/AccountApi/sendCashV2', requestData, function (xyz_data) {
            console.log(xyz_data.info)
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.cash_formSubmit(e);
                });
                return false;
            } else if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert(xyz_data.info, '/pages/user/index', 'tab');
            } else if (xyz_data.code == 5) {
                that.setData({ btn_submit_disabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(xyz_data.info);
            }
        });
    },
    /**
     * 选择所属银行
     */
    changeBankList: function (e) {
        let index = e.detail.value;
        const bank_list = this.data.bank_list;
        this.setData({
            picker_index: index,
            bank_code: bank_list[index]['code'], // 银行code
            bank_name: bank_list[index]['name'], // 银行名称
        })
    },
    chang_fendian_bind:function(){
        wx.navigateTo({
            url: '/pages/shop/mendianList/index?u_lat=' + this.data.this_u_lat+'&u_lng='+this.data.this_u_lng,
        });
    },
    //选择图片
    chooseimg_bind: function () {
      let that = this;
      wx.chooseMedia({
          count: 1,
          mediaType: ['image'],
          sizeType: ['compressed'],
          sourceType: ['album', 'camera'],
          success: function (res) {
            var requestData = {};
            _requsetCYZ.cyz_uploadPic('/Upload/image', res.tempFiles[0].tempFilePath, requestData, function (cdata) {
              let res_data = JSON.parse(cdata)
              that.setData({this_pay_img:res_data.data.file_true_path})  
            });
          }
      })
  },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    copy_weixin:function(){
        var that = this;
        wx.setClipboardData({
            data: that.data.this_shop_info_c.shop_kefu_weixin,
            success(res) {
                wx.showToast({
                    title: '客服微信复制成功',
                    icon: 'none',
                    duration: 2000
                })
            }
        })
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
})