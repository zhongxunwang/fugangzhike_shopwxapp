var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
  data: {
    user_account_info: {},
    submitIsLoading: false,
    buttonIsDisabled: false
  },
  onLoad: function () {
    var that = this;
    _functionCYZ.CYZ_loading();
    var requestData = {};
    _requsetCYZ.cyz_requestGet('/AccountApi/zhuanhuanIndex', requestData, function (cyz_data) {
      wx.hideToast();
      if (cyz_data.code == 1) {
        that.setData({
          user_account_info: cyz_data.info.user_account_info
        });
      } else if (cyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          that.onLoad();
        });
        return false;
      } else {
        _functionCYZ.CYZ_alert(cyz_data.info);
        return false;
      }
    });
  },
  pay_confirmOrder: function (e) {
    var that = this;
    that.setData({
      submitIsLoading: true,
      buttonIsDisabled: true
    });
    var rdata = e.detail.value;
    wx.showModal({
      title: '提示',
      content: '确认要转出到充值余额吗?',
      success: function (res) {
        if (res.confirm) {
          _functionCYZ.CYZ_loading();
          _requsetCYZ.cyz_requestPost('/AccountApi/confirmZhuanhuan', rdata, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
              _functionCYZ.CYZ_alert(cyz_data.info, '/pages/user/index', 'tab');
            } else if (cyz_data.code == 2) {
              that.setData({
                buttonIsDisabled: false,
                submitIsLoading: false
              });
              app.getUserDataToken(function (token) {
                that.pay_confirmOrder(e);
              });
              return false;
            } else {
              that.setData({
                buttonIsDisabled: false,
                submitIsLoading: false
              });
              app.commonErrorTips(cyz_data.info);
              return false;
            }
          });
        } else if (res.cancel) {
          that.setData({
            buttonIsDisabled: false,
            submitIsLoading: false
          });
        }
      }
    });
  }
})