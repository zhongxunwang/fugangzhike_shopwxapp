const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_from_type:0,
        this_data_list: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        is_show_gloading: true
    },
    onLoad: function (op) {
        var that = this;
        that.setData({
            this_from_type:op.from_type || 0
        })
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        requestData.from_type = that.data.this_from_type;
        _requsetCYZ.cyz_requestGet('/AppUserAccountApi/getUserChongzhiLogList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({ this_data_list: xyz_data.info, is_show_gloading: false });
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/AppUserAccountApi/getUserChongzhiLogList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_data_list;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info);
                    that.setData({ this_data_list: this_new_info_data, this_page_size: requestData.pagesize });
                }
            }
        });
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({ this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})