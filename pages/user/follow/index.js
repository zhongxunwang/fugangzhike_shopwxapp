var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        code_img:'',
        text_1:'',
        text_2:''
    },
    onLoad: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/MpPush/getWxRcode', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onLoad();
                });
            } else if (xyz_data.code == 1) {
                that.setData({
                    code_img: xyz_data.info.code_img,
                    text_1: xyz_data.info.text_1,
                    text_2: xyz_data.info.text_2
                });
            }
        });
    },
    previewCodeImg:function(){
        let _this = this;
        let img = _this.data.code_img;
        //预览图片
        wx.previewImage({
            current: img,
            urls: [img]
        })
    }
})