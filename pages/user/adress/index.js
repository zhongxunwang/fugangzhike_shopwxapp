const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
const app = getApp();
Page({
    data: {
        dish_data: [],
        this_address_data: null,
        this_address_str: '请选择地址',
        this_u_lat: 0,
        this_u_lng: 0,
        this_dish_id: 0,
        this_show_type: 0,
        this_edit_type: 'add',
        this_fapiao_leixing: 1,
        this_fapiao_info: null,
        province:'',
        city:'',
        district:'',
        region: []
    },
    onLoad: function (op) {
        this.setData({
            this_dish_id: op.dish_id
        });
        this.getMylist();
    },
    bindRegionChange: function (e) {
        let v = e.detail.value
        this.setData({
            province:v[0],
            city:v[1],
            district:v[2],
            region: v
        })
    },
    getMylist: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ApiAddress/getAddressList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_address_data: cyz_data.info
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getMylist();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //选择地图
    choose_addresss_bind: function (e) {
        var that = this;
        wx.chooseLocation({
            success: function (res) {
                console.log(res)
                that.setData({
                    this_address_str: res.address,
                    this_u_lat: res.latitude,
                    this_u_lng: res.longitude
                });
            },
            fail: function (res) {
                console.log(res)
            }
        })
    },
    //新增地址
    fapiao_add_bind: function () {
        this.setData({
            this_show_type: 1,
            this_address_str: '请选择地址',
            this_u_lat: 0,
            this_u_lng: 0
        });
    },
    //取消新增
    quxiao_add_bind: function () {
        this.setData({
            this_show_type: 0,
            this_edit_type: 'add',
            this_fapiao_leixing: 1,
            this_fapiao_info: null
        });
    },
    //切换类型
    chang_fapiao_leixing_bind: function () {
        this.setData({
            this_fapiao_leixing: this.data.this_fapiao_leixing == 1 ? 2 : 1
        });
    },
    //更新地址
    fapiao_formSubmit: function (e) {
        var that = this;
        var q_data = e.detail.value;
        q_data.fapiao_leixing = that.data.this_fapiao_leixing;
        q_data.action_type = that.data.this_edit_type;
        if (that.data.this_fapiao_info != null) {
            q_data.fapiao_id = that.data.this_fapiao_info.id;
        }
        q_data.province = that.data.province;
        q_data.city = that.data.city;
        q_data.district = that.data.district;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/ApiAddress/addAddressV2', q_data, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_show_type: 0,
                    this_edit_type: 'add',
                    this_fapiao_leixing: 1,
                    this_fapiao_info: null
                });
                that.getMylist();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.fapiao_formSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //编辑地址
    fapiao_edit_bind: function (e) {
        var that = this;
        var fapiao_id = e.currentTarget.id;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/ApiAddress/getAddresssInfo', {
            fapiao_id: fapiao_id
        }, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_fapiao_info: cyz_data.info,
                    province:cyz_data.info.province,
                    city:cyz_data.info.city,
                    district:cyz_data.info.district,
                    region: [cyz_data.info.province,cyz_data.info.city,cyz_data.info.district],
                    this_address_str: cyz_data.info.address,
                    this_u_lat: cyz_data.info.u_lat,
                    this_u_lng: cyz_data.info.u_lng,
                    this_fapiao_leixing: cyz_data.info.u_sex,
                    this_edit_type: 'edit',
                    this_show_type: 1
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.fapiao_edit_bind(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }

        });
    },
    //删除地址
    delete_fapiao_bind: function () {
        var that = this;
        var fapiao_id = that.data.this_fapiao_info.id;
        wx.showModal({
            title: '提示',
            content: "确认要删除该地址吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading();
                    _requsetCYZ.cyz_requestGet('/ApiAddress/deleteAddress', {
                        fapiao_id: fapiao_id
                    }, function (cyz_data) {
                        wx.hideToast();
                        if (cyz_data.code == 1) {
                            that.setData({
                                this_show_type: 0,
                                this_edit_type: 'add',
                                this_fapiao_leixing: 1,
                                this_fapiao_info: null
                            });
                            that.getMylist();
                        } else if (cyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.delete_fapiao_bind();
                            });
                            return false;
                        } else {
                            _functionCYZ.CYZ_alert(cyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
        
    },
    //选择收货地址
    select_fapiao_bind: function (e) {
        var fapiao_data = e.currentTarget.dataset;
        wx.setStorageSync('shop_select_address_id', fapiao_data.id);
        wx.navigateBack({
            delta: 1
        });
    }
})