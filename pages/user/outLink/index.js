const _requsetCYZ = require('../../../utils/requestData');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    copywriting: '点击链接，打开我的店',
    link: '',
    info: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //获取分享链接
    this.getShareLink()
  },
  /**
   * 获取分享链接
   */
  getShareLink() {
    _requsetCYZ.cyz_requestGet('/ShopToolApi/getShareLink', {}, (cyz_data) => {
      wx.hideToast();
      if (cyz_data.code == 1) {
        this.setData({
          info: cyz_data.info,
          link: cyz_data.info.share_link,
          copywriting: cyz_data.info.share_title
        });
      }
    });
  },
  copyHandle() {
    wx.setClipboardData({
      data: this.data.copywriting + '：' + this.data.link,
      success() {
        wx.showToast({
          title: '复制成功',
          icon: 'none'
        })
      }
    })
  }
})
