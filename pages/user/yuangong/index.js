var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_type:0,
    },
    onLoad:function(op){
        this.setData({this_type:op.type || 3});
        let title = '管理中心';
        if(this.data.this_type == 1){
            title = '经理中心';
        }else if(this.data.this_type == 2){
            title = '主管中心';
        }else if(this.data.this_type == 3){
            title = '员工中心';
        }else if(this.data.this_type == 9){
            title = '股东中心';
        }
        wx.setNavigationBarTitle({
          title: title
        })
    },
    //链接跳转
    go_url_bind: function (e) {
        if (this.data.user_is_sync == 0) {
            wx.navigateTo({
                url: '/pages/user/authorize/index',
            });
            return false;
        }
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    }
})