var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_page_id: 0,
        this_page_info: null
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.id = that.data.this_page_id;
        _requsetCYZ.cyz_requestGet('/ShopPage/getCopyInfo', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ this_page_info: xyz_data.info });
            WxParse.wxParse('article', 'html', xyz_data.info.daili_jieshao, that, 5);
        });
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //复制微信
    get_weixin_bind: function () {
        var that = this;
        wx.setClipboardData({
            data: that.data.this_page_info.daili_contact_weixin || '',
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                    icon: 'success',
                    duration: 2000
                })
            }
        })
    },
})
