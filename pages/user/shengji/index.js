const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_c_config:[],
        submitIsLoading: false,
        buttonIsDisabled: false,
        this_sj_config:null,
        this_sj_type:2,
        this_jinbi_pay_open:0,
        is_alert_content:true
    },
    go_my_dianpu:function(){
        wx.navigateTo({ url: "/pages/business_detail/index?id=" + this.data.this_c_config.shop_id});
    },
    set_alert_content_close:function(){
        this.setData({ is_alert_content:false});
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopShengji/index', requestData, function (xyz_data) {
            wx.hideToast();
            console.log(xyz_data.info)
            if (xyz_data.code == 1) {
                that.setData({ this_sj_config: xyz_data.info, this_sj_type: xyz_data.info.index_config.this_shengji_val, this_jinbi_pay_open: xyz_data.info.index_config.jinbi_is_open, this_c_config: xyz_data.info.index_config});
                WxParse.wxParse('article', 'html', xyz_data.info.index_config.shop_shengji_shuoming, that, 5);
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info, '/pages/user/index', 'tab');
                return false;
            }
            
        });
    },
    //选择充值金额
    select_rc_bind: function (e) {
        var that = this;
        let rc_config = that.data.this_sj_config.shengji_jibie;
        for (var i = 0; i < rc_config.length; i++) {
            rc_config[i].is_select = false;
        }
        rc_config[e.currentTarget.id].is_select = true;
        that.setData({ 'this_sj_config.shengji_jibie': rc_config, this_sj_type: rc_config[e.currentTarget.id].type});
    },
    //进行升级支付
    go_pay_bind:function(){
        var that = this;
        if (that.data.this_jinbi_pay_open == 1) {
            //显示余额支付
            wx.showActionSheet({
                itemList: ['金币支付', '微信支付'],
                success: (res) => {
                    if (res.tapIndex == 0) {
                        that.go_jinbi_pay_bind();
                    } else if (res.tapIndex == 1) {
                        //微信支付
                        that.go_recharge_bind();
                    }
                }, fail: () => {

                }
            });
            return false;
        } else {
            //微信支付
            that.go_recharge_bind();
            return false;
        }
    },
    //确认充值
    go_recharge_bind:function(){
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        requestData.sj_type = that.data.this_sj_type;
        _requsetCYZ.cyz_requestGet('/ShopShengji/pay', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_recharge_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        _functionCYZ.CYZ_alert('升级成功', '/pages/user/index', 'tab');
                    },
                    'fail': function (res) {

                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    go_jinbi_pay_bind:function(){
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        requestData.sj_type = that.data.this_sj_type;
        _requsetCYZ.cyz_requestGet('/ShopShengji/accountPay', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_jinbi_pay_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                _functionCYZ.CYZ_alert('升级成功', '/pages/user/index', 'tab');
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this;
        that.onShow();
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 1000);
    },
});