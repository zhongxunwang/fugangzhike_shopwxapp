const _requsetCYZ = require('../../../../utils/requestData');
const _functionCYZ = require('../../../../utils/common');
const app = getApp();
Page({
    data: {
        this_data_list: null,
        group_val: 1
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        let base_url = '';
        if (that.data.group_val == 1){
            base_url = '/ShopFenxiaoApi/myFenxiaoTeamList';
        }else{
            base_url = '/ShopFenxiaoApi/myFenxiaoTeamList_two';
        }
        _requsetCYZ.cyz_requestGet(base_url, requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_data_list: cyz_data.info });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    group_show:function(e){
        let f_level = e.currentTarget.dataset.val;
        this.setData({group_val: f_level});
        this.onShow();
    }
})