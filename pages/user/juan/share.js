const app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_quan_id: 0,
        this_data_info: {}
    },
    onLoad: function (op) {
        var that = this;
        that.setData({ this_quan_id: op.quan_id || 0 });
        that.get_index_data();
    },
    get_index_data:function(){
        let _this = this;
        var requestData = {};
        requestData.quan_id = _this.data.this_quan_id;
        _requsetCYZ.cyz_requestGet('/ShopApi/getShareQuanInfo', requestData, function (xyz_data) {
            if (xyz_data.code == 1) {
                _this.setData({ this_data_info: xyz_data.info.this_data_info });
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    _this.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //领取优惠券
    quan_lingqu_bind: function () {
        var that = this;
        _requsetCYZ.cyz_requestGet('/ShopApi/lianquShareQuan', { quan_id: that.data.this_quan_id }, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.showModal({
                    title: '提示',
                    content: cyz_data.info,
                    showCancel:false,
                    success: function (res) {
                        wx.switchTab({
                            url: '/pages/shop/shop_index/index'
                        })
                    }
                })
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.quan_lingqu_bind();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    }
})
