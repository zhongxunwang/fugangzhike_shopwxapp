const app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_quan_id: 0,
        this_data_info: {}
    },
    onLoad: function (op) {
        var that = this;
        that.setData({ this_quan_id: op.quan_id || 0 });
        that.get_index_data();
    },
    get_index_data:function(){
        let _this = this;
        var requestData = {};
        requestData.quan_id = _this.data.this_quan_id;
        _requsetCYZ.cyz_requestGet('/ShopApi/getUserQuanInfo', requestData, function (xyz_data) {
            if (xyz_data.code == 1) {
                _this.setData({ this_data_info: xyz_data.info.this_data_info });
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                  _this.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = '送您一张' + that.data.this_data_info.q_jiner_all_str;
        var sharePath = 'pages/user/juan/share?' + app.getShareUrlParams({quan_id:that.data.this_quan_id});
        return {
            title: shareTitle,
            path: sharePath
        }
    }
})