var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_current_index:0,
        this_img_data:null,
        this_login_user_id: 0,
        this_shop_info_c:null,
        this_data_share_title: '',
        this_data_share_desc: '',
        this_data_share_img: ''
    },
    set_current_index:function(e){
        this.setData({ this_current_index: e.detail.current});
    },
    onLoad: function () {
        this.get_index_data();
    },
    get_haibao_data:function(){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ImgApi/makeMyShareImgList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_user_share_img();
                });
            } else if (xyz_data.code == 1) {
                that.setData({ this_img_data: xyz_data.info });
            } else {
                app.commonErrorTips(xyz_data.info);
            }
        });
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopApi/getShopInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_login_user_id: cyz_data.info.login_user_id,
                    this_shop_info_c: cyz_data.info.shop_info_c,
                    this_data_share_title: cyz_data.info.shop_info_c.shop_title
                });
                that.getShareData();
                that.get_haibao_data();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }
        });
    },
    yulan_image_bind: function (e) {
        var that = this;
        let t_index = e.currentTarget.dataset.index;
        let this_img_data = that.data.this_img_data;
        if (this_img_data == null) {
            return false;
        }
        wx.previewImage({ current: this_img_data[t_index], urls: this_img_data });
    },
    save_image_bind: function () {
        var that = this;
        var t_index = that.data.this_current_index;
        wx.getImageInfo({
            src: that.data.this_img_data[t_index],
            success(res) {
                wx.saveImageToPhotosAlbum({
                    filePath: res.path,
                    success: function (res) {
                        if (res.errMsg == 'saveImageToPhotosAlbum:ok') {
                            wx.showToast({
                                title: '保存成功',
                                icon: 'none',
                                duration: 1000
                            });
                        }
                    },
                    fail: function (res) {
                        wx.showToast({
                            title: '保存失败，请点击图片进行保存',
                            icon: 'none',
                            duration: 1000
                        });
                    }
                })
            },
            fail: function (res) {
                wx.showToast({
                    title: '图片信息获取失败，请重试',
                    icon: 'none',
                    duration: 1000
                });
            }
        })
    },
    getShareData: function () {
        var that = this;
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/HsgShareApi/index', requestData, function (xyz_data) {
            if (xyz_data.info != null) {
                if (xyz_data.info.share_title != '') {
                    that.setData({ this_data_share_title: xyz_data.info.share_title });
                }
                if (xyz_data.info.share_desc != '') {
                    that.setData({ this_data_share_desc: xyz_data.info.share_desc });
                }
                if (xyz_data.info.share_img != '') {
                    that.setData({ this_data_share_img: xyz_data.info.share_img });
                }
            }
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var sharePath = 'pages/shop/shop_index/index?shop_id=0&tg_user_id=' + that.data.this_login_user_id;
        return {
            title: that.data.this_data_share_title,
            desc: that.data.this_data_share_desc,
            path: sharePath,
            imageUrl: that.data.this_data_share_img
        }
    }
})