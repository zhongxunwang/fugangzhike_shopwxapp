var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_order_id:0,
        this_order_info:null,
    },
    onLoad:function(op){
        this.setData({this_order_id:op.order_id});
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_id = that.data.this_order_id;
        _requsetCYZ.cyz_requestGet('/ShopOrderApi/orderInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_order_info: cyz_data.info});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //去评价
    go_pingjia_bind:function(e){
        var order_id = this.data.this_order_id
        wx.navigateTo({
            url: '../orderComment/index?order_id=' + order_id
        })
    },
    //删除订单
    delete_user_order: function (e) {
        var that = this
        var oid = e.currentTarget.id;
        wx.showModal({
            title: '提示',
            content: "确认要删除该订单吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    requestData.order_id = oid;
                    _requsetCYZ.cyz_requestGet('/ShopOrderApi/deleteUserOrder', requestData, function (xyz_data) {
                        wx.hideToast();
                        if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.delete_user_order(e);
                            });
                        } else if (xyz_data.code == 1) {
                            that.onLoad();
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
    },
    //支付订单
    go_pay_bind:function(e){
        var that = this
        var oid = e.currentTarget.id;
        wx.navigateTo({ url:"/pages/shop/pay/index?oid="+oid});
    },
    //确认收货
    go_shouhuo_bind: function () {
        var that = this;
        var order_id = this.data.this_order_id
        wx.showModal({
            title: '提示',
            content: "确认收货吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    requestData.order_id = order_id;
                    _requsetCYZ.cyz_requestGet('/ShopOrderApi/shouhuoUserOrder', requestData, function (xyz_data) {
                        wx.hideToast();
                        if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.go_shouhuo_bind();
                            });
                        } else if (xyz_data.code == 1) {
                            that.onShow();
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
    },
    initshouhuoOrderInfoData: function (data) {
        var that = this;
        if (data.code == 1) {
            _function.getOrderInfo(wx.getStorageSync("utoken"), that.data.this_order_id, that.initgetOrderInfoData, this)
        } else if (data.code == 2) {
            wx.showToast({
                title: '登陆中',
                icon: 'loading',
                duration: 10000,
                success: function () {
                    app.getNewToken(function (token) {
                        wx.hideToast();
                        _function.getOrderInfo(wx.getStorageSync("utoken"), that.data.this_order_id, that.initgetOrderInfoData, that)
                    })
                }
            });
        } else if (data.code == 5) {
            wx.showModal({
                title: '提示',
                content: data.info,
                showCancel: false
            })
            return false;
        }
    },
    //导航
    daohang_bind:function(){
        var that = this;
        var t_address = that.data.this_order_info.peisong_f_address;
        wx.openLocation({
            latitude: parseFloat(that.data.this_order_info.peisong_f_lat),
            longitude: parseFloat(that.data.this_order_info.peisong_f_lng),
            scale: 18,
            address: t_address
        });
    },
    copyExpressCode:function(e){
        wx.setClipboardData({
            data: e.currentTarget.dataset.con,
            success (res) {

            }
        })
    }
})