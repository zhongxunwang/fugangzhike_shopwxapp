const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_config_info:{},
        this_user_code: null,
    },
    onShow: function () {
        var that = this;
        wx.login({
            success: function (res) {
                that.setData({this_user_code: res.code });
            },
            fail: function (res) {
                wx.hideToast();
            }
        });
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/UserInfo/getShopConfigConfig', requestData, function (xyz_data) {
            that.setData({ 
                this_config_info: xyz_data.info.shop_c_config
            });
        });
    },
    onGetPhoneNumber: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading('操作中');
        var requestData = e.detail;
        requestData.code = that.data.this_user_code;
        console.log(requestData)
        _requsetCYZ.cyz_requestGet('/User/updateUserPhone', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.switchTab({
                    url: '/pages/user/index',
                })
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    }
})