const app = getApp();
var _requsetData = require('../../../utils/data');
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
	data: {
		is_edit:0,
		this_app_config: {},
		this_user_code: '',
		canIUseGetUserProfile: false,
		isShowPhoneBind: false,
		isAgree: true,

		nickname:'',
		avatar: ''
	},
	agreeAction: function () {
		this.setData({
			isAgree: this.data.isAgree ? false : true
		});
	},
	common_home_link: function () {
		wx.switchTab({
			url: '/pages/shop/shop_index/index',
		})
	},
	onLoad: function (op) {
		let _this = this;
		_this.setData({is_edit:op.is_edit || 0})
		let requestData = {};
		_requsetCYZ.cyz_requestPost('/WeixinMpLogin/authIndex', requestData, function (res) {
			if (res.code == 1) {
				_this.setData({
					this_app_config: res.info.this_app_config,
					nickname:res.info.this_user_info.user_nickname,
					avatar:res.info.this_user_info.user_headimg,
				});
				wx.login({
					success: function (res) {
						_this.setData({
							this_user_code: res.code
						});
					},
					fail: function (res) {
						_functionCYZ.CYZ_loading('授权登陆失败，请返回重试', 'none', 1000);
					}
				});
				if (wx.getUserProfile) {
					_this.setData({
						canIUseGetUserProfile: true
					})
				}
			} else if (res.code == 2) {
				app.getUserDataToken(function (token) {
					_this.onLoad(op);
				});
				return false;
			}
		});
	},
	//新版本登陆
	getUserProfile: function (e) {
		let form = e.detail.value
		form.avatar = this.data.avatar
		console.log(form)
		// let _this = this;
		// let _this_eeeee = e;
		// if (_this.data.isAgree == false) {
		// 	wx.showModal({
		// 		title: '提示',
		// 		content: '请先同意用户协议',
		// 		showCancel: false
		// 	});
		// 	return false;
		// }
		// wx.getUserProfile({
		// 	lang: 'zh_CN',
		// 	desc: '完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
		// 	success: (res) => {
		// 		var requestData = {};
		// 		requestData.code = _this.data.this_user_code;
		// 		requestData.user_info = JSON.stringify(res.userInfo);
		// 		_requsetCYZ.cyz_requestPost('/WeixinMpLogin/updateUserInfo', requestData, function (res) {
		// 			if (res.code == 1) {
		// 				//用户信息授权成功后，授权手机号码，重新生成code
		// 				wx.login({
		// 					success: function (res) {
		// 						_this.setData({ this_user_code: res.code, isShowPhoneBind: true });

		// 					},
		// 					fail: function (res) {
		// 						_functionCYZ.CYZ_alert(res.info);
		// 					}
		// 				});
		// 			} else if (res.code == 2) {
		// 				app.getUserDataToken(function (token) {
		// 					_this.getUserProfile(_this_eeeee);
		// 				});
		// 				return false;
		// 			}else{
		// 				_functionCYZ.CYZ_alert(res.info);
		// 			}
		// 		});
		// 	},
		// 	fail: function (res) {
		// 		_functionCYZ.CYZ_loading('授权登陆失败，请返回重试', 'none', 1000);
		// 	}
		// })
	},

	formSubmit(e) {
		let _this = this;
		let requestData = e.detail.value
		requestData.avatar = _this.data.avatar
		_requsetCYZ.cyz_requestPost('/UserInfo/saveUserBaseInfo', requestData, function (xyz_data) {
			wx.hideToast();
			if (xyz_data.code == 2) {
				app.getUserDataToken(function (token) {
					that.formSubmit(e);
				});
				return false;
			} else if (xyz_data.code == 1) {
				if(_this.data.is_edit == 1){
					wx.navigateBack();
				}else{
					//用户信息授权成功后，授权手机号码，重新生成code
					wx.login({
						success: function (res) {
							_this.setData({ this_user_code: res.code, isShowPhoneBind: true });

						},
						fail: function (res) {
							_functionCYZ.CYZ_alert(res.info);
						}
					});
				}
			} else if (xyz_data.code == 5) {
				_functionCYZ.CYZ_alert(xyz_data.info);
			}
		});
	},
	// 选择头像
	onChooseAvatar(e) {
		let _this = this
		//上传图片
		let requestData = {};
		_requsetCYZ.cyz_uploadPic('/Upload/image', e.detail.avatarUrl, requestData, function (cdata) {
			let res_data = JSON.parse(cdata)
			_this.setData({
				avatar: res_data.data.file_true_path
			})
		});
	},
	//绑定手机号码
	onGetPhoneNumber: function (e) {
		let _this = this;
		let _this_eeeee = e;
		var requestData = e.detail;
		requestData.code = _this.data.this_user_code;
		_requsetCYZ.cyz_requestPost('/WeixinMpLogin/updateUserPhone', requestData, function (res) {
			if (res.code == 1) {
				if (res.info == 1) {
					wx.redirectTo({
						url: '/pages/user/authorize/bind_referrer',
					})
					return false;
				} else {
					wx.setStorageSync('is_auth_login_back', 1);
					_this.onNavigateBack(1);
				}
			} else if (res.code == 2) {
				app.getUserDataToken(function (token) {
					_this.onGetPhoneNumber(_this_eeeee);
				});
				return false;
			} else {
				_functionCYZ.CYZ_alert(res.info);
			}

		});
	},
	onNavigateBack(delta = 1) {
		const pages = getCurrentPages()
		if (pages.length > 1) {
			wx.navigateBack({
				delta: Number(delta || 1)
			})
		} else {
			wx.switchTab({
				url: '/pages/shop/shop_index/index',
			})
		}
	}
})
