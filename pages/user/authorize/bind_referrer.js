var app = getApp();
var _requsetData = require('../../../utils/data');
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_user_info: {},
        submitIsLoading: false,
        buttonIsDisabled: false,
        yzm_btn_disabled: true,
        yzm_btn_text: '获取验证码',
        yzm_all_time: 60,
        this_user_phone: '',
        phone_yzm_code: ''
    },
    formSubmit: function (e) {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var rdata = e.detail.value;
        _requsetCYZ.cyz_requestPost('/User/bindReferrerUser', rdata, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.formSubmit(e);
                });
            } else if (xyz_data.code == 1) {
                wx.showModal({
                    title: '提示',
                    showCancel:false,
                    content: xyz_data.info,
                    success: function (res) {
                      wx.setStorageSync('is_auth_login_back', 1);
                      wx.navigateBack();
                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                that.setData({ submitIsLoading: false, buttonIsDisabled: false });
                return false;
            }
        });
    },
    //验证手机号码
    check_phone_bind: function (e) {
        var that = this
        var phone_v = e.detail.value
        if (!(/^1\d{10}$/.test(phone_v))) {
            
        } else {
          console.log(11)
            that.setData({
                yzm_btn_disabled: false,
                this_user_phone: phone_v
            })
        }
    },
    //发送验证码
    send_phone_code_bind: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.phone = that.data.this_user_phone;
        _requsetCYZ.cyz_requestGet('/UserRenzheng/sendUserPhoneTrue', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.send_phone_code_bind();
                });
            } else if (xyz_data.code == 1) {
                //倒计时
                that.setData({yzm_btn_disabled: true});
                that.getShengTime();
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
            }
        });
    },
    getShengTime: function () {
        var that = this
        var yijing_time = that.data.yzm_all_time - 1;
        if (that.data.yzm_all_time > 0) {
            that.setData({
                yzm_all_time: yijing_time,
                yzm_btn_text: '等待' + yijing_time + '秒'
            })
            setTimeout(function () {
                that.getShengTime();
            }
                , 1000)
        } else {
            that.setData({
                yzm_btn_disabled: false,
                yzm_btn_text: '获取验证码',
                yzm_all_time: 60
            })
        }
    }
})