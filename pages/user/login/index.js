var app = getApp();
var _requsetData = require('../../../utils/data');
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        
    },
    onUserInfo: function (e) {
        var that = this;
        console.log(e);
        const detail = e.detail;
        if (!detail.userInfo) {
            _functionCYZ.CYZ_alert('请同意授权');
        } else {
            var that = this;
            var info = detail.userInfo;
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.u_data = JSON.stringify(info);
            _requsetCYZ.cyz_requestPost('/User/updateUserInfo', requestData, function (xyz_data) {
                wx.hideToast();
                if (xyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.onUserInfo(e);
                    });
                } else if (xyz_data.code == 1){
                    wx.navigateBack();
                }else{
                    _functionCYZ.CYZ_alert('授权失败，请重试授权');
                }
            });
        }
    }
})