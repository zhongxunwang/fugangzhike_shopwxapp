const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        submitIsLoading: false,
        buttonIsDisabled: false,
        this_user_info:{},
        this_user_data:null,
        this_rc_config:null,
        this_rc_index:null,
        this_rc_jiner:0,
        this_input_jiner:null,
        is_self_open:0

    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/HsgRechargeApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_info: cyz_data.info.user_info,
                    this_user_data: cyz_data.info.user_account_info,
                    this_rc_config: cyz_data.info.cz_config,
                    is_self_open:cyz_data.info.is_self_open
                });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            }
        });
    },
    //选择充值金额
    select_rc_bind:function(e){
        var that = this;
        var tindex = e.currentTarget.dataset.index;
        let rc_config = that.data.this_rc_config;
        for(var i=0;i<rc_config.length;i++){
            rc_config[i].is_select = false;
        }
        if (tindex == -1){
            that.setData({ this_rc_config: rc_config, this_rc_index: -1 });
        }else{
            rc_config[tindex].is_select = true;
            that.setData({ this_rc_config: rc_config, this_rc_index: tindex, this_rc_jiner: rc_config[tindex].rc_man });
        }
    },
    get_rc_jiner:function(e){
        var that = this;
        let rz_jiner = e.detail.value;
        that.setData({ this_rc_jiner: rz_jiner, this_input_jiner: rz_jiner});

    },
    //确认充值
    go_recharge_bind:function(){
        var that = this;
        let cz_jiner = parseFloat(that.data.this_rc_jiner);
        if (cz_jiner <= 0){
            wx.showModal({
                title: '提示',
                content: "请选择或输入充值金额",
                showCancel: false
            });
            return false;
        }
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        var requestData = {};
        requestData.rz_account = cz_jiner;
        requestData.version = 'v2';
        _requsetCYZ.cyz_requestGet('/HsgRechargeApi/recharge', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.go_recharge_bind();
                });
                return false;
            } else if (xyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': xyz_data.info.timeStamp,
                    'nonceStr': xyz_data.info.nonceStr,
                    'package': xyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': xyz_data.info.paySign,
                    'success': function (res) {
                        _functionCYZ.CYZ_alert('充值成功', '/pages/user/index', 'tab');
                    },
                    'fail': function (res) {

                    }
                })
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this;
        that.onShow();
        setTimeout(() => {
            wx.stopPullDownRefresh();
        }, 1000);
    },
});