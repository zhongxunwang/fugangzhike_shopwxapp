var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_g_op:{},
        this_order_list: [],
        hasMore: false,
        is_loadmore: true,
        this_page: 1,//当前页码
        pagesize: 10,//每页数量
        this_nav_name: 'index',
        glo_is_load: true,
        group_val: 'all'
    },
    onLoad: function (op) {
        var that = this;
        if (op.hasOwnProperty('s_type')) {
            that.setData({ group_val: op.s_type, this_g_op:op});
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page;
        requestData.pagenum = that.data.pagesize;
        requestData.group_val = that.data.group_val;
        _requsetCYZ.cyz_requestGet('/ShopOrderVcardApi/getUserOrderList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onLoad(op);
                });
            } else if (xyz_data.code == 1) {
                if (xyz_data.info == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.length < that.data.pagesize) {
                        that.setData({ is_loadmore: false });
                    }
                }
                that.setData({ this_order_list: xyz_data.info });
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page + 1;
        requestData.pagenum = that.data.pagesize;
        requestData.group_val = that.data.group_val;
        _requsetCYZ.cyz_requestGet('/ShopOrderVcardApi/getUserOrderList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.info == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
                var this_new_info_data = that.data.this_order_list;
                this_new_info_data = this_new_info_data.concat(xyz_data.info);
                that.setData({ this_order_list: this_new_info_data, this_page: requestData.pagesize });
            }
        });
    },
    //公用页面跳转
    go_base_url_bind: function (e) {
        let t_url = e.currentTarget.dataset.url;
        wx.navigateTo({ url: t_url });
    },
    group_show: function (e) {
        var that = this;
        that.setData({ group_val: e.target.dataset.val, this_page: 1, is_loadmore: true})
        that.onLoad({});
    },
    //删除订单
    delete_user_order: function (e) {
        var that = this
        var oid = e.currentTarget.id;
        wx.showModal({
            title: '提示',
            content: "确认要删除该订单吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    requestData.order_id = oid;
                    _requsetCYZ.cyz_requestGet('/ShopOrderApi/deleteUserOrder', requestData, function (xyz_data) {
                        wx.hideToast();
                        if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.delete_user_order(e);
                            });
                        } else if (xyz_data.code == 1) {
                            that.onLoad(that.data.this_g_op);
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        })
    },
})