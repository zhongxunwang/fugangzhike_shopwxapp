const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_group_config: null,
        this_group_list:null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        isShowComment:false,
        this_gid:0,
        this_g_index:0
    },
    onLoad: function () {
        app.checkUserGIsName();
        this.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/HsgGroupApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.this_group_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.this_group_list.length < 10) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({
                this_group_config:cyz_data.info.this_group_config,
                this_group_list:cyz_data.info.this_group_list
            });
            // wx.setNavigationBarTitle({
            //     title: cyz_data.info.index_data.shop_name
            // });
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/HsgGroupApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.this_group_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.this_group_list.length < 10) {
                    that.setData({ is_loadmore: false });
                }
                that.setData({ this_group_list: that.data.this_group_list.concat(cyz_data.info.this_group_list), this_page_size: requestData.pagesize });
            }
        });
    },
    /**
     * 跳转页面
     */
    onAppNavigateTap: function (e) {
        const dataset = e.detail.target ? e.detail.target.dataset : e.currentTarget.dataset;
        const url = dataset.url, appurl = dataset.appurl, atype = dataset.atype || 1, appId = dataset.appid;
        if (atype == 1) {
            wx.navigateTo({
                url: url, fail: () => {
                    wx.switchTab({
                        url: url,
                    });
                }
            });
        } else if (atype == 2) {
            wx.navigateToMiniProgram({ appId: appId, path: appurl });
        }
    },
    //图片放大
    img_max_bind: function (e) {
        var that = this;
        var img_max_url = e.currentTarget.dataset.url;
        var this_img_key = e.currentTarget.dataset.key;
        var this_data_key = e.currentTarget.dataset.okey;
        let turls = that.data.this_group_list[this_data_key].g_imgs;
        wx.previewImage({
            current: img_max_url,
            urls: turls
        })
    },
    //点赞
    goods_like_bind: function (e) {
        var that = this;
        let this_glist = that.data.this_group_list;
        let this_gdata = this_glist[e.currentTarget.dataset.index];
        if (this_gdata.user_is_like == 1){
            return false;
        }
        console.log(this_gdata);
        _functionCYZ.CYZ_loading_n("操作中");
        var requestData = {};
        requestData.g_id = e.currentTarget.dataset.id;
        requestData.g_type = 1;
        _requsetCYZ.cyz_requestGet('/HsgGroupApi/groupAction', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                this_glist[e.currentTarget.dataset.index].user_is_like = 1;
                this_glist[e.currentTarget.dataset.index].like_num = parseInt(this_gdata.like_num) + 1;
                if (this_glist[e.currentTarget.dataset.index].g_like_list == null){
                    this_glist[e.currentTarget.dataset.index].g_like_list = xyz_data.info;
                }else{
                    this_glist[e.currentTarget.dataset.index].g_like_list = this_glist[e.currentTarget.dataset.index].g_like_list.concat(xyz_data.info);
                }
                that.setData({ this_group_list: this_glist});
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.goods_like_bind(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    showCommentSubmit:function(e){
        this.setData({this_gid:e.currentTarget.dataset.id,this_g_index:e.currentTarget.dataset.index,isShowComment:true});
    },
    hideCommentSubmit:function(){
        this.setData({ this_gid: 0, this_g_index: 0, isShowComment: false });
    },
    /**
	 * 提交评论
	 */
    onCommentSubmit: function (e) {
        var that = this;
        let this_glist = that.data.this_group_list;
        let this_gdata = this_glist[that.data.this_g_index];

        _functionCYZ.CYZ_loading_n("请稍候...");
        var requestData = e.detail.value;
        requestData.g_id = that.data.this_gid;
        _requsetCYZ.cyz_requestGet('/HsgGroupApi/addComment', requestData, function (xyz_data) {
            wx.hideLoading();
            if (xyz_data.code == 1) {
                if (this_glist[that.data.this_g_index].g_comment_list == null){
                    this_glist[that.data.this_g_index].g_comment_list = xyz_data.info;
                }else{
                    this_glist[that.data.this_g_index].g_comment_list = this_glist[that.data.this_g_index].g_comment_list.concat(xyz_data.info);
                }
                that.setData({ this_gid: 0, this_g_index: 0, isShowComment: false, this_group_list:this_glist});
                return true;
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onCommentSubmit(e);
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_group_config.group_share_title||'朋友圈';
        var shareImg = that.data.this_group_config.group_share_img || '';
        var sharePath = 'pages/group/index';
        return {
            title: shareTitle,
            imageUrl: shareImg,
            path: sharePath
        }
    }
})