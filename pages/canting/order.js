const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
const cart_storage_key = 'this_canting_shop_cart_data';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    this_peisong_type:0,
    this_fendian_id:0,
    this_table_id: 0,
    this_table_name:'',
    this_table_info:{},
    is_show_table_layer:false,
    this_table_list:[],
    this_cart_list:[],
    all_cart_number:0,
    all_cart_amount:0,
    table_goods_list:[],
    table_order_info:{},
    table_order_id:0,
    submitIsDisabled:false,
    submitIsLoading:false,
    this_renshu_data:[],
    user_address_info:null,
    index: -1,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      this_table_id:options.table_id || 0,
      this_peisong_type:options.peisong_type || 0,
      this_fendian_id:options.fendian_id || 0
    })
  },
  onShow(){
    this.getIndexData();
  },
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },
  select_address_bind:function(){
    wx.navigateTo({
        url: '/pages/user/adress/index'
    });
    return false;
  },
  getIndexData: function () {
    let _this = this;
    let address_id = wx.getStorageSync("shop_select_address_id") || 0;
    let cart_data = wx.getStorageSync(cart_storage_key) || {};
    _functionCYZ.CYZ_loading();
    var requestData = {};
    requestData.peisong_type = _this.data.this_peisong_type;
    requestData.table_id = _this.data.this_table_id;
    requestData.fendian_id = _this.data.this_fendian_id;
    requestData.address_id = address_id;
    requestData.cart_data = JSON.stringify(cart_data);
    _requsetCYZ.cyz_requestPost('/CantingIndex/checkOrder', requestData, function (xyz_data) {
      wx.hideToast();
      if (xyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          _this.getIndexData();
        });
        return false;
      }
      _this.setData({
        this_cart_list: xyz_data.data.this_cart_list,
        all_cart_number:xyz_data.data.all_cart_number,
        all_cart_amount:_this.returnFloat(xyz_data.data.all_cart_amount),
        this_table_info:xyz_data.data.this_table_info,
        table_goods_list:xyz_data.data.table_goods_list,
        table_order_info:xyz_data.data.table_order_info,
        table_order_id:xyz_data.data.table_order_id,
        user_address_info:xyz_data.data.user_address_info,
        this_renshu_data:xyz_data.data.this_renshu_data
      });
      if(xyz_data.data.this_table_info){
        _this.setData({
          this_table_id: xyz_data.data.this_table_info.id,
          this_table_name:xyz_data.data.this_table_info.table_name
        });
      }
    });
  },
  show_table_layout_bind:function(){
    let _this = this;
    if(_this.data.table_order_id > 0){
      return false;
    }
    _functionCYZ.CYZ_loading();
    var requestData = {};
    requestData.fendian_id = _this.data.this_fendian_id;
    _requsetCYZ.cyz_requestPost('/CantingIndex/getTableList', requestData, function (xyz_data) {
      wx.hideToast();
      if (xyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          _this.show_table_layout_bind();
        });
        return false;
      }
      _this.setData({
        this_table_list: xyz_data.data.this_table_list,
        is_show_table_layer:true
      });
    });

  },
  hide_table_layout_bind:function(){
    this.setData({is_show_table_layer:false});
  },
  //选择桌号
  selectTable_bind: function(e) {
    let _this = this;
    _this.setData({
      this_table_id:_this.data.this_table_list[e.detail.value].id,
      this_table_name:_this.data.this_table_list[e.detail.value].table_name,
    })
    _this.getIndexData();
    _this.hide_table_layout_bind();
  },
  goBack:function(){
    wx.navigateBack();
  },
  goJiacaiAction:function(){
    let _this = this;
    wx.redirectTo({
      url:"/pages/canting/index?table_id=" + _this.data.this_table_id + "&fendian_id=" + _this.data.this_fendian_id+'&is_jiacai_action=1'
    })
  },
  //加数量
  bind_cart_number_jia_one:function(e){
    let _this = this;
    let car_list = _this.data.this_cart_list;
    let this_index = e.currentTarget.dataset.index;
    let key_str = '';
    let cart_data = {};
    //当前购物车的数据
    let base_card_data = wx.getStorageSync(cart_storage_key) || {};

    if (car_list[this_index].goods_sku_id == '') {
      key_str = car_list[this_index].goods_id + '_id';
      cart_data = base_card_data[key_str] || {};
      if (Object.keys(cart_data).length > 0) {
        cart_data.cart_goods_number = parseInt(cart_data.cart_goods_number) + 1;
      }
    } else {
      key_str = car_list[this_index].goods_id  + '_' + car_list[this_index].goods_sku_id ;
      cart_data = base_card_data[key_str] || {};

      if (Object.keys(cart_data).length > 0) {
        cart_data.cart_goods_number = parseInt(cart_data.cart_goods_number) + 1;
      }
    }
    base_card_data[key_str] = cart_data;
    wx.setStorageSync(cart_storage_key, base_card_data);
    _this.getIndexData();
  },
  bind_cart_number_jian_one:function(e){
    let _this = this;
    let car_list = _this.data.this_cart_list;
    let this_index = e.currentTarget.dataset.index;
    let key_str = '';
    let cart_data = {};
    //当前购物车的数据
    let base_card_data = wx.getStorageSync(cart_storage_key) || {};

    if (car_list[this_index].goods_sku_id == '') {
      key_str = car_list[this_index].goods_id + '_id';
      cart_data = base_card_data[key_str] || {};
      if (Object.keys(cart_data).length > 0) {
        if (cart_data.cart_goods_number <= 1) {
          delete base_card_data[key_str];
        } else {
          cart_data.cart_goods_number = parseInt(cart_data.cart_goods_number) - 1;
          base_card_data[key_str] = cart_data;
        }
      }
    } else {
      key_str = car_list[this_index].goods_id  + '_' + car_list[this_index].goods_sku_id ;
      cart_data = base_card_data[key_str] || {};

      if (Object.keys(cart_data).length > 0) {
        if (cart_data.cart_goods_number <= 1) {
          delete base_card_data[key_str];
        } else {
          cart_data.cart_goods_number = parseInt(cart_data.cart_goods_number) - 1;
          base_card_data[key_str] = cart_data;
        }
      }
    }
    wx.setStorageSync(cart_storage_key, base_card_data);
    _this.getIndexData();
  },
  //确认下单
  createOrder:function(){
    let _this = this;
    if(_this.data.submitIsLoading) return;
    _this.setData({submitIsDisabled: true, submitIsLoading: true });
    _functionCYZ.CYZ_loading();
    let cart_data = wx.getStorageSync(cart_storage_key) || {};
    var requestData = {};
    requestData.peisong_type = _this.data.this_peisong_type;
    requestData.table_id = _this.data.this_table_id;
    requestData.fendian_id = _this.data.this_fendian_id;
    requestData.renshu_index = _this.data.index;
    requestData.table_order_id = _this.data.table_order_id;
    requestData.user_address_info = JSON.stringify(_this.data.user_address_info);
    requestData.cart_data = JSON.stringify(cart_data);
    _requsetCYZ.cyz_requestPost('/CantingIndex/postOrder', requestData, function (cyz_data) {
      wx.hideToast();
      if (cyz_data.code == 1) {
        //清空商品
        wx.setStorageSync(cart_storage_key, null);
        _this.setData({ submitIsDisabled: false, submitIsLoading: false,table_order_id: cyz_data.info.order_id});
        if(_this.data.this_peisong_type == 0){
          wx.showModal({
            title: '提示',
            content: "下单成功",
            showCancel:false,
            success: function (res) {
              _this.getIndexData();
            }
          })
          return false;
        }else{
          wx.redirectTo({
            url: '/pages/shop/pay/index?oid=' + cyz_data.info.order_id
          })
          return false;
        }
      } else if (cyz_data.code == 2) {
        app.getUserDataToken(function (token) {
          _this.createOrder();
        });
      } else {
        _functionCYZ.CYZ_alert(cyz_data.info);
      }
      _this.setData({ submitIsDisabled: false, submitIsLoading: false });
    });
  },
  createPayAction:function(){
    let _this = this;
    //是否有待结算商品
    let cart_data = wx.getStorageSync(cart_storage_key) || {};
    if(Object.keys(cart_data).length > 0){
      wx.showModal({
        title: '提示',
        content: "有未下单的商品，确认要去支付吗？",
        success: function (res) {
          if (res.confirm == true) {
            wx.redirectTo({
              url: '/pages/shop/pay/index?oid=' + _this.data.table_order_id
            })
          }
        }
      })
    }else{
      wx.redirectTo({
        url: '/pages/shop/pay/index?oid=' + _this.data.table_order_id
      })
    }
  },
  returnFloat:function (value) {
    if (value <= 0) return '0.00';
    value = Math.round(parseFloat(value) * 100) / 100;
    return value.toFixed(2);
  }
})