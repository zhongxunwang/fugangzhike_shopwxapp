const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        this_swiper_data:[],
        this_data_list: [],
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        listdata:[1,2,3,4,5,6]
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopTehui/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            that.setData({ 
                this_data_list: xyz_data.info.shop_goods,
                this_swiper_data:xyz_data.info.shop_swiper
            });
        });
    },
    /**
     * 跳转页面
     */
    onAppNavigateTap: function (e) {
        const dataset = e.detail.target ? e.detail.target.dataset : e.currentTarget.dataset;
        const url = dataset.url,
            appurl = dataset.appurl,
            atype = dataset.atype || 1,
            appId = dataset.appid;
        if (atype == 1) {
            wx.navigateTo({
                url: url,
                fail: () => {
                    wx.switchTab({
                        url: url,
                    });
                }
            });
        } else if (atype == 2) {
            wx.navigateToMiniProgram({
                appId: appId,
                path: appurl
            });
        }
    },
    //商品详情跳转
    go_goods_info_bind: function (e) {
        wx.navigateTo({
            url: '/pages/shop/detail/index?goods_id=' + e.currentTarget.id
        });
    }
})