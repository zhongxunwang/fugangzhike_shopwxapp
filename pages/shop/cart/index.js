const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_shop_id:0,
        cart_list: [],
        all_g_number: 0,
        all_g_price: 0,
        all_g_yunfei: 0,
        this_check_val: [],
        all_is_checked: false,
        btn_mall_sure_disabled: false,
        glo_is_load: true,
        all_goods_price: 0,
        all_goods_number: 0,
        is_show_gloading: true
    },
    bind_go_home: function () {
        wx.switchTab({
            url: '/pages/shop/shop_index/index'
        })
    },
    mallsure: function () {
        var that = this
        if (that.data.cart_list == null) {
            wx.showModal({
                title: '提示',
                content: '对不起,购物车暂无商品',
                showCancel: false
            })
            return
        }
        if (that.data.all_goods_number == 0) {
            wx.showModal({
                title: '提示',
                content: '对不起,请选择要结算的商品',
                showCancel: false
            })
            return
        }
        var cart_info = []
        for (var i = 0; i < that.data.cart_list.length; i++) {
            cart_info[i] = { 'goodid': '', 'goodstatus': '' }
            cart_info[i].goodid = that.data.cart_list[i].id
            console.log(that.data.this_check_val)
            if (that.data.this_check_val.indexOf(i) > -1 || that.data.this_check_val.indexOf(i + '') > -1) {
                cart_info[i].goodstatus = 1
            } else {
                cart_info[i].goodstatus = 0
            }
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.cart_info = cart_info;
        _requsetCYZ.cyz_requestGet('/ShopCartApi/setCartStatus', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.navigateTo({
                    url: '../submit/index?shop_id=' + that.data.this_shop_id
                })
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.mallsure();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    onLoad:function(op){
        if(op.shop_id != undefined){
            this.setData({ this_shop_id: op.shop_id});
        }
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.shop_id = that.data.this_shop_id;
        _requsetCYZ.cyz_requestGet('/ShopCartApi/getCartListV2', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    cart_list: cyz_data.info.glist,
                    all_g_number: cyz_data.info.all_g_number,
                    all_g_price: cyz_data.info.all_g_price,
                    all_g_yunfei: cyz_data.info.all_g_yunfei,
                    all_goods_price: 0,
                    all_goods_number: 0,
                    all_is_checked: false,
                    this_check_val: [],
                    is_show_gloading:false
                })
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //全选
    all_checkboxChange: function (e) {
        var that = this
        var isallcheck = false
        if (e.detail.value[0] == 1) {
            isallcheck = true
            that.setData({
                all_goods_price: that.data.all_g_price,
                all_goods_number: that.data.all_g_number
            })
        } else {
            isallcheck = false
            that.setData({
                all_goods_price: 0,
                all_goods_number: 0,
            })
        }
        var datas = that.data.cart_list
        var check_val = []
        for (var i = 0; i < datas.length; i++) {
            datas[i].is_checked = isallcheck
            if (isallcheck == true) {
                check_val[i] = i
            }
        }
        that.setData({
            cart_list: datas,
            this_check_val: check_val,
        })
    },
    //单选
    single_checkboxChange: function (e) {
        var that = this
        var price = 0
        var num = 0
        for (var i = 0; i < e.detail.value.length; i++) {
            price += that.data.cart_list[e.detail.value[i]].goods_number * that.data.cart_list[e.detail.value[i]].goods_price
            num += that.data.cart_list[e.detail.value[i]].goods_number * 1
        }
        that.setData({
            all_goods_price: price.toFixed(2),
            all_goods_number: num
        })
        var c_length = e.detail.value.length
        var g_length = that.data.cart_list.length
        if (c_length >= g_length) {
            that.setData({
                all_is_checked: true,
                this_check_val: e.detail.value,

            })
        } else {
            that.setData({
                all_is_checked: false,
                this_check_val: e.detail.value
            })
        }
    },
    change_price1: function (e) {
        var that = this
    },
    //减少数量
    bind_cart_number_jian: function (e) {
        var that = this
        var this_cart_id = e.currentTarget.id
        var datas = that.data.cart_list
        for (var i = 0; i < datas.length; i++) {
            if (datas[i].id == this_cart_id) {
                if (datas[i].goods_number > 1) {
                    datas[i].goods_number = parseInt(datas[i].goods_number) - 1
                } else {
                    datas[i].goods_number = 1
                }
                _functionCYZ.CYZ_loading();
                var requestData = {};
                requestData.cid = this_cart_id;
                requestData.cnum = datas[i].goods_number;
                _requsetCYZ.cyz_requestGet('/ShopCartApi/editCartList', requestData, function (cyz_data) {
                    wx.hideToast();
                    if (cyz_data.code == 1) {
                        that.onShow();
                    } else if (cyz_data.code == 2) {
                        app.getUserDataToken(function (token) {
                            that.bind_cart_number_jian(e);
                        });
                        return false;
                    } else {
                        _functionCYZ.CYZ_alert(cyz_data.info);
                        return false;
                    }
                });
            }
        }
        that.setData({
            all_goods_price: 0,
            all_goods_number: 0,
            all_is_checked: false,
            this_check_val: e.detail.value
        })
    },
    //增加数量
    bind_cart_number_jia: function (e) {
        var that = this
        var this_cart_id = e.currentTarget.id
        var datas = that.data.cart_list
        for (var i = 0; i < datas.length; i++) {
            if (datas[i].id == this_cart_id) {
                datas[i].goods_number = parseInt(datas[i].goods_number) + 1;
                _functionCYZ.CYZ_loading();
                var requestData = {};
                requestData.cid = this_cart_id;
                requestData.cnum = datas[i].goods_number;
                _requsetCYZ.cyz_requestGet('/ShopCartApi/editCartList', requestData, function (cyz_data) {
                    wx.hideToast();
                    if (cyz_data.code == 1) {
                        that.onShow();
                    } else if (cyz_data.code == 2) {
                        app.getUserDataToken(function (token) {
                            that.bind_cart_number_jia(e);
                        });
                        return false;
                    } else {
                        _functionCYZ.CYZ_alert(cyz_data.info);
                        return false;
                    }
                }); 
            }
        }
        that.setData({
            all_goods_price: 0,
            all_goods_number: 0,
            all_is_checked: false,
            this_check_val: e.detail.value
        })
    },
    //删除购物车
    bind_delete_cart: function () {
        var that = this
        if (that.data.all_goods_number == 0) {
            wx.showModal({
                title: '提示',
                content: '请选择要删除的商品',
                showCancel: false,
            })
            return false;
        }

        wx.showModal({
            title: '提示',
            content: "确认要删除该商品吗?",
            success: function (res) {
                if (res.confirm == true) {
                    var del_good_id = [];
                    for (var i = 0; i < that.data.this_check_val.length; i++) {
                        del_good_id[i] = that.data.cart_list[that.data.this_check_val[i]].id
                    }
                    _functionCYZ.CYZ_loading();
                    var requestData = {};
                    requestData.cid = del_good_id.toString();
                    _requsetCYZ.cyz_requestGet('/ShopCartApi/delCartList', requestData, function (cyz_data) {
                        wx.hideToast();
                        if (cyz_data.code == 1) {
                            that.onShow();
                        } else if (cyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.bind_cart_number_jia(e);
                            });
                            return false;
                        } else {
                            _functionCYZ.CYZ_alert(cyz_data.info);
                            return false;
                        }
                    }); 
                }
            }
        })

    },
    //下拉刷新
    onPullDownRefresh: function () {
        var that = this
        _function.getCartList(wx.getStorageSync("utoken"), that.initgetCartListData, this)
        that.setData({
            all_is_checked: false,
            all_goods_price: 0,
            all_goods_number: 0,
        })
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000)
    },
})