const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_data_list: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
    },
    onLoad: function () {
        var that = this;
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopApi/getLiveList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            that.setData({this_data_list: xyz_data.info.index_live_list});
        });
    },
    // 跳转直播间
    gotoLive: function (e) {
        let _this = this;
        let this_index = e.currentTarget.dataset.index;
        let this_data = _this.data.this_data_list[this_index];
        // 填写具体的房间号，可通过下面【获取直播房间列表】 API 获取
        let roomId = [this_data.room_id];
        // 开发者在直播间页面路径上携带自定义参数（如示例中的path和pid参数），后续可以在分享卡片链接和跳转至商详页时获取，详见【获取自定义参数】、【直播间到商详页面携带参数】章节（上限600个字符，超过部分会被截断）
        let customParams = encodeURIComponent(JSON.stringify({
            path: 'pages/index/index',
        }))
        wx.navigateTo({
            url: `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${roomId}&custom_params=${customParams}`
        })
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})