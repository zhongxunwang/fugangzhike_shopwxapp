var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
      this_user_info:null,
        this_c_config: null,
        submitIsLoading: false,
        buttonIsDisabled: false,
        is_beizhu_show: false,
      isShowUserShouquan:false,
    },
    change_beizhu_show: function () {
        this.setData({ is_beizhu_show: this.data.is_beizhu_show ? false : true });
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopMaidan/getMaidaiConfig', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ 
                  this_c_config: cyz_data.info,
                  this_user_info:cyz_data.info.user_info
                });
              if (cyz_data.info.user_info.user_gender == 9){
                  that.setData({ isShowUserShouquan:true});
              }
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    pay_confirmOrder: function (e) {
        var that = this;
        that.setData({ submitIsLoading: true, buttonIsDisabled: true });
        let pay_type = e.detail.value.pay_name;
        var rdata = e.detail.value;
        if (pay_type == 1) {
            return false;
        } else {
            _functionCYZ.CYZ_loading();
            _requsetCYZ.cyz_requestPost('/ShopMaidanOff/index', rdata, function (cyz_data) {
                wx.hideToast();
                if (cyz_data.code == 1) {
                    wx.requestPayment({
                        'timeStamp': cyz_data.info.timeStamp,
                        'nonceStr': cyz_data.info.nonceStr,
                        'package': cyz_data.info.package,
                        'signType': 'MD5',
                        'paySign': cyz_data.info.paySign,
                        'success': function (res) {
                            wx.redirectTo({
                                url: '/pages/shop/maidan_off/success?oid=' + cyz_data.info.order_sn
                            });    
                        },
                        'fail': function (res) {
                            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                        },
                        'complete': function () {
                            that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                        }
                    });
                } else if (cyz_data.code == 2) {
                    that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                    app.getUserDataToken(function (token) {
                        that.pay_confirmOrder(e);
                    });
                    return false;
                } else {
                    that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                    app.commonErrorTips(cyz_data.info);
                    return false;
                }
            });
        }
    },
    //用户信息授权
    onUserInfo: function (e) {
        var that = this;
        const detail = e.detail;
        if (!detail.userInfo) {
            _functionCYZ.CYZ_alert('为了提供更好的服务，请点击允许哦');
            return false;
        } else {
            var that = this;
            var info = detail.userInfo;
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.u_data = JSON.stringify(info);
            _requsetCYZ.cyz_requestPost('/User/updateUserInfo', requestData, function (xyz_data) {
                wx.hideToast();
                if (xyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.onShow();
                    });
                } else if (xyz_data.code == 1) {
                    that.setData({ isShowUserShouquan: false });
                    that.onShow();
                } else {
                    _functionCYZ.CYZ_alert('授权失败，请重试授权');
                }
            });
        }
    },
})
