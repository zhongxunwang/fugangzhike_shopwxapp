var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_data_list:null,
        this_u_lat:0,
        this_u_lng:0
    },
    onLoad: function (op) {
        this.setData({ this_u_lat: op.u_lat || 0,this_u_lng:op.u_lng || 0 });
        this.get_user_location()
    },
    get_user_location:function(){
        const that = this
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                that.setData({ this_u_lng: res.longitude, this_u_lat: res.latitude });
                that.get_index_data();
            },
            fail: function () {
                that.get_index_data();
            }
        })
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/ShopApi/getFendianList', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ this_data_list: xyz_data.info });
        });
    },
    chang_fendian_bind:function(e){
        let fendian_id = e.currentTarget.dataset.id;
        wx.setStorageSync('order_fendian_id', fendian_id);
        wx.navigateBack();
    }
})
