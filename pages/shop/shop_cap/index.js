var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
        select_type: '',
        select_jiage_type: '',
        select_xiaoliang_type: '',
        is_select_jiage: false,
        is_select_xiaoliang:false,
        this_goods_data:null,
        this_one_cate_data:null,
        this_two_cate_data:null,
        this_cate_id:0,
        this_two_cate_id:0
    },
    select_cate_id:function(e){
        let t_id = e.currentTarget.dataset.id;
        this.setData({ this_cate_id: t_id, this_two_cate_id:0});
        this.onLoad();
    },
    select_two_cate_id:function(e){
        let t_id = e.currentTarget.dataset.id;
        this.setData({ this_two_cate_id: t_id });
        this.onLoad();
    },
    select_goods_list: function (e) {
        var that = this;
        var s_type = e.currentTarget.dataset.stype;
        that.setData({ select_jiage_type: '' });
        if (s_type == 'jiage') {
            if (that.data.is_select_jiage == true) {
                that.setData({ select_jiage_type: 'jiage_sheng', is_select_jiage: false });
            } else {
                that.setData({ select_jiage_type: 'jiage_jiang', is_select_jiage: true });
            }
        }
        if (s_type == 'xiaoliang') {
            if (that.data.is_select_xiaoliang == true) {
                that.setData({ select_xiaoliang_type: 'xiaoliang_sheng', is_select_xiaoliang: false });
            } else {
                that.setData({ select_xiaoliang_type: 'xiaoliang_jiang', is_select_xiaoliang: true });
            }
        }
        that.setData({ select_type: s_type, this_page_size: 1, is_loadmore: true });
        that.getIndexData();
    },
    onLoad:function(){
        this.setData({ this_page_size: 1, is_loadmore: true });
        this.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.cate_id = that.data.this_cate_id;
        requestData.two_cate_id = that.data.this_two_cate_id;
        requestData.pagesize = 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.stype = that.data.select_type;
        requestData.stype_jiage = that.data.select_jiage_type;
        requestData.stype_xiaoliang = that.data.select_xiaoliang_type;
        _requsetCYZ.cyz_requestGet('/HsgGoodsCateApi/getNewIndex', requestData, function (xyz_data) {
            wx.hideToast();
            that.setData({ 
                this_one_cate_data: xyz_data.info.one_cate_list,
                this_goods_data: xyz_data.info.goods_list,
                this_two_cate_data: xyz_data.info.two_cate_list
            });

            if (xyz_data.info.goods_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.goods_list.length < 10) {
                    that.setData({ is_loadmore: false });
                }
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        var requestData = {};
        requestData.cate_id = that.data.this_cate_id;
        requestData.two_cate_id = that.data.this_two_cate_id;
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.stype = that.data.select_type;
        requestData.stype_jiage = that.data.select_jiage_type;
        requestData.stype_xiaoliang = that.data.select_xiaoliang_type;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/HsgGoodsCateApi/getNewIndex', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.goods_list == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.goods_list.length < 10) {
                    that.setData({ is_loadmore: false });
                }
                that.setData({ this_goods_data: that.data.this_goods_data.concat(cyz_data.info.goods_list), this_page_size: requestData.pagesize });
            }
        });
    },
})
