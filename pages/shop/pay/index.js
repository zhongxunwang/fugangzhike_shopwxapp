const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_order_id:0,
        this_is_daifu:0,
        this_order_info:null,
        isShowPayRcode:false,
        pay_img:''
    },
    onLoad: function (op) {
        console.log(op.oid)
        let _this = this,scene = app.getSceneData(op);
        let order_id = 0;
        let is_daifu = 0;
        if (op.oid != undefined) {
            order_id = op.oid;
        }else{
            order_id = scene.order_id || 0;
            is_daifu = scene.is_daifu || 0;
        }
        _this.setData({this_order_id: order_id,this_is_daifu:is_daifu});
    },
    onShow: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_id = that.data.this_order_id;
        _requsetCYZ.cyz_requestGet('/ShopOrderApi/orderPayInfo', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_order_info: cyz_data.info})
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    daifuACtion:function(){
        let _this = this;
        //生成待付二维码
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_id = _this.data.this_order_id;
        _requsetCYZ.cyz_requestGet('/ShopOrderApi/getDaifuWxrcode', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                _this.setData({pay_img:cyz_data.info.pay_img,isShowPayRcode:true});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    _this.onShow();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });

    },
    closeDaifuACtion:function(){
        let _this = this;
        _this.setData({isShowPayRcode:false});
    },
    //开始支付
    pay_confirmOrder: function (e) {
        var that = this;
        let pay_type = e.detail.value.pay_type;
        that.setData({buttonIsDisabled: true,submitIsLoading: true});
        if(pay_type == 1){//微信支付
            that.pay_weixin_do(pay_type);return false;
        } else if (pay_type == 2){//余额支付
            that.pay_yuer_do(pay_type);return false;
        } else if (pay_type == 3) {//货到付款
            that.pay_yuer_do(pay_type); return false;
        }else{
            wx.showToast({title: '请选择支付方式',duration: 20000});
        }
    },
    pay_weixin_do: function (pay_type){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pay_type = pay_type;
        requestData.order_id = that.data.this_order_id;
        requestData.is_daifu = that.data.this_is_daifu;
        _requsetCYZ.cyz_requestGet('/ShopOrderApi/orderPay', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.requestPayment({
                    'timeStamp': cyz_data.info.timeStamp,
                    'nonceStr': cyz_data.info.nonceStr,
                    'package': cyz_data.info.package,
                    'signType': 'MD5',
                    'paySign': cyz_data.info.paySign,
                    'success': function (res) {
                        wx.redirectTo({
                            url: '/pages/shop/success/index?order_sn=' + cyz_data.info.order_sn
                        });
                    },
                    'fail': function (res) {
                        that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                    },
                    'complete': function () {
                        that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                    }
                });
            } else if (cyz_data.code == 2) {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                app.getUserDataToken(function (token) {
                    that.pay_weixin_do(pay_type);
                });
                return false;
            } else {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //余额支付
    pay_yuer_do: function (pay_type){
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pay_type = pay_type;
        requestData.order_id = that.data.this_order_id;
        requestData.is_daifu = that.data.this_is_daifu;
        _requsetCYZ.cyz_requestGet('/ShopOrderApi/yuerPay', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.redirectTo({
                    url: '/pages/shop/success/index?order_sn=' + cyz_data.info.order_sn
                });
            } else if (cyz_data.code == 2) {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                app.getUserDataToken(function (token) {
                    that.pay_yuer_do(pay_type);
                });
                return false;
            } else {
                that.setData({ buttonIsDisabled: false, submitIsLoading: false });
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    }
})