const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_order_sn: null,
        this_user_info:null,
        this_order_info: null,
        this_share_data:null,
        this_login_user_id: 0,
        this_data_share_title: '',
        this_data_share_desc: '',
		this_data_share_img: '',
		isShowQrcode:false,
		qrcode_setting:{}
    },
    onLoad: function (op) {
        var that = this;
        let order_sn = op.order_sn;
        that.setData({ this_order_sn: order_sn });
        that.get_index_data();
	},
	closeQrcode(){
		this.setData({isShowQrcode: false})
	},
    get_index_data: function () {
		// TODO 显示营销二维码
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.order_sn = that.data.this_order_sn;
        _requsetCYZ.cyz_requestGet('/HsgSuccessApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_user_info: cyz_data.info.user_info,
                    this_order_info: cyz_data.info.order_info,
					this_login_user_id: cyz_data.info.user_info.id,
					qrcode_setting:cyz_data.info.qrcode_setting
				});
				if(cyz_data.info.qrcode_setting.is_show_qr_code == 1){
					that.setData({isShowQrcode:true})
				}
                that.getShareData();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    getShareData: function () {
        var that = this;
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/HsgShareApi/index', requestData, function (xyz_data) {
            if (xyz_data.info != null) {
                if (xyz_data.info.share_title != '') {
                    that.setData({ this_data_share_title: xyz_data.info.share_title });
                }
                if (xyz_data.info.share_img != '') {
                    that.setData({ this_data_share_img: xyz_data.info.share_img });
                }
            }
        });
	},
	previewImage () {
        var that = this;
        var showImg=that.data.qrcode_setting.qr_code_img;
		wx.previewImage({
			urls: [showImg],
		})
    },
    onShareAppMessage: function () {
        var that = this;
        var sharePath = 'pages/shop/shop_index/index?shop_id=0&tg_user_id=' + that.data.this_login_user_id;
        console.log(sharePath)
        return {
            title: that.data.this_data_share_title,
            path: sharePath,
            imageUrl: that.data.this_data_share_img
        }
    },
})