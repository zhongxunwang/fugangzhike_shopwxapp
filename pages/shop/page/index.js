var app = getApp();
var _requsetCYZ = require('../../../utils/requestData');
var _functionCYZ = require('../../../utils/common.js');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_page_id: 0,
        this_page_info:null,
        this_login_user_id:0
    },
    onLoad:function(op){
        this.setData({this_page_id:op.id || 0});
        this.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.id = that.data.this_page_id;
        _requsetCYZ.cyz_requestGet('/ShopPage/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 1) {
                that.setData({ this_page_info: xyz_data.info });
                wx.setNavigationBarTitle({
                    title: xyz_data.info.page_title || '介绍'
                });
                WxParse.wxParse('article', 'html', xyz_data.info.page_content, that, 5);
            } else if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onShow();
                });
            }
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var sharePath = 'pages/shop/page/index?id='+ that.data.this_page_id +'&tg_user_id=' + that.data.this_page_info.login_user_id;
        return {
            title: that.data.this_page_info.page_title,
            path: sharePath
        }
    },
    videoPlayAction:function(e){
        let _this = this;
        if(!_this.data.video_is_play){
            var requestData = {};
            requestData.shop_id = 0;
            requestData.goods_id = 0;
            requestData.video_url = _this.data.this_page_info.page_video;
            _requsetCYZ.cyz_requestPost('/ShopFlowApi/addFlowLog', requestData, function (res) {
                wx.hideToast();
                if (res.code == 1) {
                    _this.setData({video_is_play:true})
                } else if (res.code == 2) {
                    app.getUserDataToken(function (token) {
                        _this.videoPlayAction(e);
                    });
                    return false;
                } else {
                    // _functionCYZ.CYZ_alert(res.info);
                    // return false;
                }
            });
        }
    }
})
