const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_zt_id: 0,
        this_cate_id:0,
        this_cate_list:null,
        this_keywords: '',
        this_page_size: 1,
        this_page_num: 10,
        goods_data: null,
        list_type: true,
        select_type: '',
        select_jiage_type: '',
        is_select_jiage: false,
        is_loadmore: true
    },
    onLoad: function (op) {
        var that = this;
        let zt_id = op.zid || 0;
        that.setData({ this_zt_id: zt_id});
        that.getIndexData();
    },
    go_goods_this_bind:function(e){
        var that = this;
        that.setData({ this_cate_id: e.currentTarget.id});
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        var requestData = {};
        requestData.zt_id = that.data.this_zt_id;
        requestData.pagesize = 1;
        requestData.pagenum = that.data.this_page_num;
        requestData.keywords = that.data.this_keywords;
        requestData.stype = that.data.select_type;
        requestData.stype_jiage = that.data.select_jiage_type;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/ShopApi/getZtGoodsList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.length < 10) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({ goods_data: cyz_data.info});
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        var requestData = {};
        requestData.zt_id = that.data.this_zt_id;
        requestData.pagesize = that.data.this_page_size + 1;;
        requestData.pagenum = that.data.this_page_num;
        requestData.keywords = that.data.this_keywords;
        requestData.stype = that.data.select_type;
        requestData.stype_jiage = that.data.select_jiage_type;
        _functionCYZ.CYZ_loading();
        _requsetCYZ.cyz_requestGet('/ShopApi/getZtGoodsList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info == null) {
                that.setData({is_loadmore: false });
            } else {
                if (cyz_data.info.length < 10) {
                    that.setData({is_loadmore: false });
                }
                that.setData({ goods_data: that.data.goods_data.concat(cyz_data.info), this_page_size: requestData.pagesize});
            }
        });
    },
    select_goods_list: function (e) {
        var that = this;
        var s_type = e.currentTarget.dataset.stype;
        that.setData({ select_jiage_type: '' });
        if (s_type == 'jiage') {
            if (that.data.is_select_jiage == true) {
                that.setData({ select_jiage_type: 'jiage_sheng', is_select_jiage: false });
            } else {
                that.setData({ select_jiage_type: 'jiage_jiang', is_select_jiage: true });
            }
        }
        that.setData({ select_type: s_type, this_page_size: 1, is_loadmore: true });
        that.getIndexData();
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
    //商品列表跳转
    go_goods_list_bind: function (e) {
        wx.navigateTo({ url: "../category/index?cid=" + e.currentTarget.id });
    },
    //商品详情跳转
    go_goods_info_bind: function (e) {
        wx.navigateTo({ url: '../detail/index?goods_id=' + e.currentTarget.id });
    },
    onPullDownRefresh: function () {
        var that = this;
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = "网上渔药批发中心";
        var sharePath = 'pages/shop/zhuanti/index?zid=' + that.data.this_zt_id;
        return {
            title: shareTitle,
            desc: '',
            path: sharePath
        }
    }
})