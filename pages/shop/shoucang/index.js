const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common.js');
Page({
    data: {
        this_data_list: null,
        this_page_size: 1,
        this_page_num: 10,
        is_loadmore: true,
    },
    onLoad: function () {
        var that = this;
        that.getIndexData();
    },
    getIndexData: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserShoucangList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            }
            if (xyz_data.info == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.length < that.data.this_page_num) {
                    that.setData({is_loadmore: false });
                }
            }
            that.setData({this_data_list: xyz_data.info});
        });
    },
    //取消收藏
    shoucang_info_bind: function (e) {
        var that = this;
        wx.showModal({
            title: '提示',
            content: "确认要取消收藏吗?",
            success: function (res) {
                if (res.confirm == true) {
                    _functionCYZ.CYZ_loading_n("加载中");
                    var requestData = {};
                    requestData.goods_id = e.currentTarget.dataset.id;
                    requestData.action_type = e.currentTarget.dataset.type;
                    _requsetCYZ.cyz_requestGet('/ShopApi/goodsShoucang', requestData, function (xyz_data) {
                        wx.hideLoading();
                        if (xyz_data.code == 1) {
                            that.getIndexData();
                            return true;
                        } else if (xyz_data.code == 2) {
                            app.getUserDataToken(function (token) {
                                that.shoucang_info_bind(e);
                            });
                            return false;
                        } else {
                            _functionCYZ.CYZ_alert(xyz_data.info);
                            return false;
                        }
                    });
                }
            }
        });
    },
    onReachBottom: function (e) {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/ShopShuju/getUserShoucangList', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.getIndexData();
                });
                return false;
            } else if (xyz_data.code == 1) {
                if (xyz_data.info == null) {
                    that.setData({ is_loadmore: false });
                } else {
                    if (xyz_data.info.length < that.data.this_page_num) {
                        that.setData({ is_loadmore: false });
                    }
                    var this_new_info_data = that.data.this_data_list;
                    this_new_info_data = this_new_info_data.concat(xyz_data.info);
                    that.setData({ this_data_list: this_new_info_data, this_page_size: requestData.pagesize });
                }
            }
        });
    },
    /**
     * 跳转页面
     */
    onNavigateTap: function (e) {
        const dataset = e.currentTarget.dataset, url = dataset.url, type = dataset.type;
        const nav = { url: url };
        if ("switch" == type) {
            nav.fail = function () {
                wx.navigateTo({ url: url });
            };
            wx.switchTab(nav);
        } else {
            wx.navigateTo(nav);
        }
    },
    onPullDownRefresh: function () {
        var that = this;
        that.setData({this_page_size: 1, is_loadmore: true });
        that.getIndexData();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    }
})