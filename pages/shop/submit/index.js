const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_shop_id: 0,
        cart_list: [],
        all_g_number: 0,
        all_price: 0,
        all_g_price: 0,
        all_g_yunfei: 0,
        gudong_youhui_price:0,
        this_all_order_price: 0,
        this_all_order_jian_price: 0,
        this_address_id: 0,
        this_address_info: '请选择',
        wx_address_info: null,
        btn_submit_disabled: false,
        submitIsLoading: false,
        no_is_check: false,
        is_show_quan_status: false,
        this_user_is_quan: 0,
        this_shop_quan_list: null,
        this_quan_select_id: 0,
        this_quan_select_jiner: 0,
        this_quan_select_str: '请选择',
        this_peisong_type: 1,//配送类型1快递2自取
        this_u_lng: 0,
        this_u_lat: 0,
        this_shop_config: null,
        this_fendian_id:0,
        this_fendian_info:null,
        this_form_list:null
    },
    chang_fendian_bind:function(){
        wx.navigateTo({
            url: '../mendianList/index?u_lat=' + this.data.this_u_lat+'&u_lng='+this.data.this_u_lng,
        });
    },
    chang_peisong_type: function (e) {
        let ptype = e.currentTarget.dataset.tval;
        this.setData({ this_peisong_type: ptype });
        this.getAllOrderPrice();
    },
    get_user_location: function () {
        let that = this;
        if (wx.getStorageSync("order_user_lat") && wx.getStorageSync("order_user_lng")){
            that.setData({ this_u_lng: wx.getStorageSync("order_user_lng"), this_u_lat: wx.getStorageSync("order_user_lat") });
            that.get_index_data();return false;
        }
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                wx.setStorageSync('order_user_lat', res.latitude);
                wx.setStorageSync('order_user_lng', res.longitude);
                that.setData({ this_u_lng: res.longitude, this_u_lat: res.latitude });
                that.get_index_data();
            },
            fail: function () {
                that.get_index_data();
            }
        })
    },
    get_index_data: function () {
        var that = this;
        var address_id = wx.getStorageSync("shop_select_address_id");
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.data = 'is_mallsure';
        requestData.address_id = address_id || 0;
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        requestData.fendian_id = that.data.this_fendian_id||0;
        _requsetCYZ.cyz_requestGet('/ShopCartApi/getCartListV2', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    cart_list: cyz_data.info.glist,
                    all_g_number: cyz_data.info.all_g_number,
                    all_g_price: cyz_data.info.all_g_price,
                    all_price: cyz_data.info.all_g_price,
                    all_g_yunfei: cyz_data.info.all_g_yunfei,
                    gudong_youhui_price:that.returnFloat(cyz_data.info.gudong_youhui_price),
                    this_user_is_quan: cyz_data.info.user_is_quan,
                    this_all_order_price: that.returnFloat(cyz_data.info.all_g_price) + that.returnFloat(cyz_data.info.all_g_yunfei),
                    this_shop_config: cyz_data.info.shop_i_config,
                    this_peisong_type: cyz_data.info.shop_i_config.this_peisong_type,
                    this_fendian_info: cyz_data.info.shop_fendian_info,
                    this_form_list: cyz_data.info.shop_form_list,
                    wx_address_info: cyz_data.info.user_address_info
                });
                that.getAllOrderPrice();
                //that.getAddressInfo();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    onShow: function () {
        if (wx.getStorageSync('order_fendian_id') > 0){
            this.setData({ this_fendian_id: wx.getStorageSync('order_fendian_id')});
        }
        this.get_user_location();
    },
    //选择收货地址
    select_address_bind: function () {
        var that = this;
        wx.navigateTo({
            url: '../../user/adress/index'
        });
        return false;
    },
    //获取用户收货地址
    getAddressInfo: function () {
        var that = this;
        var aid = wx.getStorageSync("shop_select_address_id");
        if (aid && aid > 0) {
            _functionCYZ.CYZ_loading();
            var requestData = {};
            requestData.fapiao_id = aid;
            _requsetCYZ.cyz_requestGet('/ApiAddress/getAddresssInfo', requestData, function (cyz_data) {
                wx.hideToast();
                if (cyz_data.code == 1) {
                    var wx_address = {};
                    wx_address.userName = cyz_data.info.consignee;
                    wx_address.telNumber = cyz_data.info.mobile;
                    wx_address.detailInfo = cyz_data.info.address + cyz_data.info.buchong;
                    wx_address.u_lat = cyz_data.info.u_lat;
                    wx_address.u_lng = cyz_data.info.u_lng;
                    that.setData({ wx_address_info: wx_address });
                } else if (cyz_data.code == 2) {
                    app.getUserDataToken(function (token) {
                        that.getAddressInfo();
                    });
                    return false;
                } else {
                    _functionCYZ.CYZ_alert(cyz_data.info);
                    return false;
                }
            });
        }
    },
    //选择代金券
    select_quan_bind: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.shop_id = that.data.this_shop_id;
        requestData.quan_status = 0;
        requestData.limit_jiner = that.data.all_g_price;
        _requsetCYZ.cyz_requestGet('/ShopApi/getUserQuanList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                let quan_data = cyz_data.info;
                if (quan_data == null) {
                    that.setData({ is_show_quan_status: true, this_shop_quan_list: quan_data, no_is_check: false });
                    return false;
                }
                for (var i = 0; i < quan_data.length; i++) {
                    if (quan_data[i].id == that.data.this_quan_select_id) {
                        quan_data[i].is_check = true;
                    }
                }
                that.setData({ is_show_quan_status: true, this_shop_quan_list: quan_data, no_is_check: false });
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.select_quan_bind();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },
    //确认代金券
    select_quan_id_bind: function (e) {
        let this_q_id = e.currentTarget.id;
        if (this_q_id == 0) {
            this.setData({ this_quan_select_id: 0, this_quan_select_jiner: 0, this_quan_select_str: '请选择' });
            this.close_quan_status();
            return false;
        }
        let this_q_jiner = e.currentTarget.dataset.jiner;
        let this_q_name = e.currentTarget.dataset.name;
        this.setData({ this_quan_select_id: this_q_id, this_quan_select_jiner: this_q_jiner, this_quan_select_str: this_q_name + '(' + this_q_jiner + '元)' });
        this.close_quan_status();
    },
    //关闭优惠券页
    close_quan_status: function () {
        //更新订单价格
        var that = this;
        let all_jiage = that.data.this_all_order_price - that.returnFloat(that.data.this_quan_select_jiner);
        all_jiage = that.returnFloat(all_jiage);
        this.setData({is_show_quan_status: false });
        this.getAllOrderPrice();
    },
    getAllOrderPrice:function(){
        let _this = this;
        let yunfei = 0;
        if(_this.data.this_peisong_type == 1){
            yunfei = parseFloat(_this.data.all_g_yunfei);
        }else if(_this.data.this_peisong_type == 2){
            yunfei = 0;
        }
        let all_jiage = parseFloat(_this.data.all_g_price) + yunfei - parseFloat(_this.data.this_quan_select_jiner) - parseFloat(_this.data.gudong_youhui_price);
        all_jiage = _this.returnFloat(all_jiage);
        this.setData({this_all_order_jian_price:all_jiage});
    },
    //提交订单
    order_formSubmit: function (e) {
        var that = this;
        _functionCYZ.CYZ_loading();
        that.setData({btn_submit_disabled: true, submitIsLoading: true });
        var order_info = e.detail.value;
        order_info.wx_address = JSON.stringify(that.data.wx_address_info);
        order_info.form_id = e.detail.formId;
        order_info.shop_id = that.data.this_shop_id;
        order_info.quan_id = that.data.this_quan_select_id;
        order_info.quan_jiner = that.data.this_quan_select_jiner;
        order_info.gudong_youhui_price = that.data.gudong_youhui_price;
        order_info.peisong_type = that.data.this_peisong_type;
        order_info.peisong_fendian_id = that.data.this_fendian_info.id;
        let this_form_data = that.data.this_form_list;
        if (this_form_data != null && this_form_data != ''){
            for (var i = 0; i < this_form_data.length; i++) {
                const item = this_form_data[i];
                if (item.f_type == 5) {
                    order_info[item.f_biaoshi] = item.f_default_value.join(' ');
                } else {
                    order_info[item.f_biaoshi] = item.f_default_value;
                }
            }
        }
        _requsetCYZ.cyz_requestPost('/ShopOrderApi/postOrderV2', order_info, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                wx.redirectTo({
                    url: '../pay/index?oid=' + cyz_data.info
                });
                return false;
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.order_formSubmit(e);
                });
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
            }
            that.setData({ btn_submit_disabled: false, submitIsLoading: false });
        });
    },
    returnFloat: function (value) {
        var value = Math.round(parseFloat(value) * 100) / 100;
        return value.toFixed(2);
    },


    /*********自定义表单处理*********/
    onFormDateChange:function(e){
        this.changeFormValue(e);
    },
    onFormBlur:function(e){
        this.changeFormValue(e);
    },
    changeFormValue:function(e){
        let that = this;
        let control;
        let old_form_list = that.data.this_form_list;
        const dataset = e.currentTarget.dataset, value = e.detail.value, name = e.currentTarget.id, refresh = dataset.refresh;
        for (var i = 0; i < old_form_list.length; i++) {
            if (old_form_list[i].f_biaoshi == name){
                if (old_form_list[i].f_type == '5') {
                    let stype = dataset.type, datetime = old_form_list[i].f_default_value;
                    if (stype == 'date') datetime[0] = value;
                    else datetime[1] = value;
                } else {
                    old_form_list[i].f_default_value = that.isBoolean(value) ? (value ? 1 : 0) : value;
                }
                break;
            }
        }
        if (refresh) {
            that.setData({this_form_list: old_form_list });
        }
    },
    isBoolean:function(val) {
        return val === true || val === false;
    }
})