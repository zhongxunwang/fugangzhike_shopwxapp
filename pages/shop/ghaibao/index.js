const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
var WxParse = require('../../../wxParse/wxParse.js');
Page({
    data: {
        this_goods_id: 0,
        this_img_data:null
    },
    onLoad: function (op) {
        var that = this;
        let goods_id = 0;
        if (op.id != undefined) {
            goods_id = op.id;
        }
        that.setData({ this_goods_id: goods_id });
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.goods_id = goods_id || 0;
        _requsetCYZ.cyz_requestGet('/ApiHaibao/makeGoodsShareImg', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({ this_img_data: cyz_data.info});
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.onLoad(op);
                });
            }else{
                wx.showToast({
                    title: cyz_data.info,
                    icon: '',
                    duration: 1000
                });
            }
        });
    },
    yulan_image_bind: function () {
        let this_img_data = this.data.this_img_data;
        if (this_img_data == null) {
            return false;
        }
        wx.previewImage({ current: this_img_data, urls: [this_img_data] });
    },
    save_image_bind: function () {
        var that = this;
        if (that.data.this_img_data == null) {
            wx.showToast({
                title: '图片信息获取失败，请重试',
                icon: 'none',
                duration: 1000
            });
            return false;
        }
        wx.getImageInfo({
            src: that.data.this_img_data,
            success(res) {
                wx.saveImageToPhotosAlbum({
                    filePath: res.path,
                    success: function (res) {
                        if (res.errMsg == 'saveImageToPhotosAlbum:ok') {
                            wx.showToast({
                                title: '保存成功',
                                icon: 'none',
                                duration: 1000
                            });
                        }
                    },
                    fail: function (res) {
                        wx.showToast({
                            title: '保存失败，请点击图片进行保存',
                            icon: 'none',
                            duration: 1000
                        });
                    }
                })
            },
            fail: function (res) {
                wx.showToast({
                    title: '图片信息获取失败，请重试',
                    icon: 'none',
                    duration: 1000
                });
            }
        })
    }
})