const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_form_id:0,
        this_form_info:{},
        this_field_list:[],
        buttonIsDisabled:false,
        submitIsLoading:false
    },
    onLoad:function(op){
        this.setData({this_form_id:op.form_id || 0})
        this.get_index_data()
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.form_id = that.data.this_form_id;
        _requsetCYZ.cyz_requestGet('/HsgFormApi/getCustomFormData', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_form_info:cyz_data.info.this_form_info,
                    this_field_list:cyz_data.info.this_field_list
                });
                wx.setNavigationBarTitle({
                  title: cyz_data.info.this_form_info.title,
                })
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
                return false;
            }
        });
    },

    /*********自定义表单处理*********/
    onFormDateChange:function(e){
        this.changeFormValue(e);
    },
    onFormBlur:function(e){
        this.changeFormValue(e);
    },
    changeFormValue:function(e){
        let that = this;
        let control;
        let old_form_list = that.data.this_field_list;
        const dataset = e.currentTarget.dataset, value = e.detail.value, name = e.currentTarget.id, refresh = dataset.refresh;
        for (var i = 0; i < old_form_list.length; i++) {
            if (old_form_list[i].f_biaoshi == name){
                if (old_form_list[i].f_type == '5') {
                    let stype = dataset.type, datetime = old_form_list[i].f_default_value;
                    if (stype == 'date') datetime[0] = value;
                    else datetime[1] = value;
                } else {
                    old_form_list[i].f_default_value = that.isBoolean(value) ? (value ? 1 : 0) : value;
                }
                break;
            } 
        }
        if (refresh) {
            that.setData({this_field_list: old_form_list });
        }
    },
    isBoolean:function(val) {
        return val === true || val === false;
    },
    order_formSubmit:function(e){
        let that = this
        var order_info = {};
        order_info.form_id = that.data.this_form_id;
        _functionCYZ.CYZ_loading();
        that.setData({btn_submit_disabled: true, submitIsLoading: true });
        let this_form_data = that.data.this_field_list;
        if (this_form_data != null && this_form_data != ''){
            for (var i = 0; i < this_form_data.length; i++) {
                const item = this_form_data[i];
                if (item.f_type == 5) {
                    order_info[item.f_biaoshi] = item.f_default_value.join(' ');
                } else {
                    order_info[item.f_biaoshi] = item.f_default_value;
                }
            }
        }
        _requsetCYZ.cyz_requestPost('/HsgFormApi/postCustomFormData', order_info, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                _functionCYZ.CYZ_alert(cyz_data.info, '/pages/user/index', 'tab');
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.order_formSubmit(e);
                });
            } else {
                _functionCYZ.CYZ_alert(cyz_data.info);
            }
            that.setData({ btn_submit_disabled: false, submitIsLoading: false });
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var sharePath = 'pages/shop/custom_form/index?form_id=' + that.data.this_form_id;
        return {
            title: that.data.this_form_info.title,
            path: sharePath
        }
    },
})