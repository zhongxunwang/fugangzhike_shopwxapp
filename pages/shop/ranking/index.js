const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        slect_type: 1,
        this_page_size: 1,
        this_page_num: 15,
        is_loadmore: true,
        this_index_data: null
    },
    onShow: function () {
        var that = this;
        that.setData({ slect_type: that.data.slect_type, is_loadmore: true, this_page_size: 1 });
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.slect_type = that.data.slect_type;
        requestData.pagesize = that.data.this_page_size;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/HsgRankingApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.info.index_list_data == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (cyz_data.info.index_list_data.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
            }
            that.setData({ this_index_data: cyz_data.info.index_list_data });
        });
    },
    onReachBottom: function () {
        var that = this;
        if (that.data.is_loadmore == false) {
            return false;
        }
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.slect_type = that.data.slect_type;
        requestData.pagesize = that.data.this_page_size + 1;
        requestData.pagenum = that.data.this_page_num;
        _requsetCYZ.cyz_requestGet('/HsgRankingApi/index', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.info.index_list_data == null) {
                that.setData({ is_loadmore: false });
            } else {
                if (xyz_data.info.index_list_data.length < that.data.this_page_num) {
                    that.setData({ is_loadmore: false });
                }
                var this_new_info_data = that.data.this_index_data;
                this_new_info_data = this_new_info_data.concat(xyz_data.info.index_list_data);
                that.setData({ this_index_data: this_new_info_data, this_page_size: requestData.pagesize });
            }
        });
    },
    //切换选项卡
    onTabChangeTap: function (e) {
        const that = this;
        var index = e.currentTarget.dataset.type;
        that.setData({ slect_type: index, is_loadmore: true,this_page_size: 1 });
        that.onShow();//加载数据
    }
})