const app = getApp();
const _requsetCYZ = require('../../../utils/requestData');
const _functionCYZ = require('../../../utils/common');
Page({
    data: {
        this_quan_list:null,
        this_data_share_title: '',
        this_data_share_desc: '',
        this_data_share_img: ''
    },
    onLoad:function(){
        this.get_index_data();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/ShopApi/getShopQuanList', requestData, function (cyz_data) {
            wx.hideToast();
            if (cyz_data.code == 1) {
                that.setData({
                    this_quan_list:cyz_data.info
                });
                that.getShareData();
            } else if (cyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.get_index_data();
                });
                return false;
            }

        });
    },
    lingqu_action_bind:function(e){
        var that = this;
        console.log(e)
        let qid = e.currentTarget.dataset.id;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.qid = qid;
        _requsetCYZ.cyz_requestGet('/ShopApi/ShopQuanLingqu', requestData, function (xyz_data) {
            wx.hideToast();
            if (xyz_data.code == 2) {
                app.getUserDataToken(function (token) {
                    that.quan_lingqu_bind(e);
                });
            } else if (xyz_data.code == 1) {
                wx.showModal({
                    title: '提示',
                    content: "领取优惠券成功",
                    showCancel: false,
                    success: function (res) {
                        that.get_index_data();
                    }
                });

            } else {
                _functionCYZ.CYZ_alert(xyz_data.info);
                return false;
            }
        });
    },
    go_home_bind: function () {
        wx.switchTab({
            url: '/pages/shop/shop_index/index',
            fail: function () {
                wx.navigateTo({
                    url: '/pages/shop/shop_index/index',
                })
            }
        })
    },
    getShareData: function () {
        var that = this;
        var requestData = {};
        _requsetCYZ.cyz_requestGet('/HsgShareApi/index', requestData, function (xyz_data) {
            if (xyz_data.info != null) {
                if (xyz_data.info.share_title != '') {
                    that.setData({
                        this_data_share_title: xyz_data.info.quan_share_title
                    });
                }
                if (xyz_data.info.share_img != '') {
                    that.setData({
                        this_data_share_img: xyz_data.info.quan_share_img
                    });
                }
            }
        });
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.this_data_share_title;
        var sharePath = 'pages/shop/get-redbag/index?' + app.getShareUrlParams();
        return {
            title: shareTitle,
            path: sharePath,
            imageUrl: that.data.this_data_share_img
        }
    },
})