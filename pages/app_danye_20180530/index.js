const app = getApp();
const _requsetCYZ = require('../../utils/requestData');
const _functionCYZ = require('../../utils/common.js');
var WxParse = require('../../wxParse/wxParse.js');
Page({
    data: {
        this_u_lat: 0,
        this_u_lng: 0,
        d_info: null,
        this_c_config:null,
        this_fuwu_info:null,
        is_show_alert:false,
        is_show_gloading:true
    },
    onLoad: function () {
        this.get_index_data();
        // this.getLocation();
    },
    get_index_data: function () {
        var that = this;
        _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.u_lat = that.data.this_u_lat;
        requestData.u_lng = that.data.this_u_lng;
        _requsetCYZ.cyz_requestGet('/DanyeApi/index', requestData, function (cyz_data) {
            wx.hideToast();
            that.setData({
                d_info: cyz_data.info.index_data,
                this_c_config: cyz_data.info.index_c_config,
                is_show_gloading:false
            });
            WxParse.wxParse('article', 'html', cyz_data.info.index_data.shop_jianjie, that, 5);
            wx.setNavigationBarTitle({
                title: cyz_data.info.index_data.shop_name
            });
        });
    },
    getLocation: function () {
        var that = this;
        _functionCYZ.CYZ_loading("定位中，请稍候...");
        wx.getLocation({
            type: 'gcj02',
            success: function (res) {
                that.setData({this_u_lat: res.latitude, this_u_lng: res.longitude });
                that.get_index_data();
            },
            fail: function () {
                //弹出系统设置
                // wx.openSetting({
                //     success: (res) => {
                //         if (res.authSetting['scope.userLocation'] == false) {
                //             wx.showModal({
                //                 title: '提示',
                //                 content: "请允许地理位置授权",
                //                 showCancel: false,
                //                 success: function () {
                //                     that.getLocation();
                //                 }
                //             });
                //         } else {
                //             that.getLocation();
                //         }
                //     }
                // });
                // return false;
                that.get_index_data();
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    },
    /**
     * 跳转页面
     */
    onAppNavigateTap: function (e) {
        const dataset = e.detail.target ? e.detail.target.dataset : e.currentTarget.dataset;
        const url = dataset.url, appurl = dataset.appurl, atype = dataset.atype || 1, appId = dataset.appid;
        if (atype == 1) {
            wx.navigateTo({
                url: url, fail: () => {
                    wx.switchTab({
                        url: url,
                    });
                }
            });
        } else if (atype == 2) {
            wx.navigateToMiniProgram({ appId: appId, path: appurl });
        }
    },
    /**
	 * 拨打电话
	 */
    onCallTap: function (e) {
        const mobile = e.currentTarget.dataset.mobile;
        wx.makePhoneCall({
            phoneNumber: mobile,
        });
    },
    //地图跳转
    go_map_info: function () {
        var that = this;
        wx.openLocation({
            latitude: parseFloat(that.data.d_info.shop_lat),
            longitude: parseFloat(that.data.d_info.shop_lng),
            scale: 18,
            address: that.data.d_info.shop_address
        });
    },
    //复制微信
    get_weixin_bind: function () {
        var that = this;
        wx.setClipboardData({
            data: that.data.d_info.shop_wenxin || '',
            success: function (res) {
                wx.showToast({
                    title: '复制成功',
                    icon: 'success',
                    duration: 2000
                })
            }
        })
    },
    //服务详情
    show_fuwu_info_bind:function(e){
        var that = this;
        let fuwu_id = e.currentTarget.dataset.id;
        let shop_id = that.data.d_info.id;
        // _functionCYZ.CYZ_loading();
        var requestData = {};
        requestData.fuwu_id = fuwu_id;
        requestData.shop_id = shop_id;
        _requsetCYZ.cyz_requestGet('/DanyeApi/getFuwuInfo', requestData, function (cyz_data) {
            // wx.hideToast();
            that.setData({
                this_fuwu_info:cyz_data.info
            });
            if (cyz_data.info.is_show_alert == 1){
                WxParse.wxParse('article_fuwu', 'html', cyz_data.info.f_content, that, 5);
                that.setData({is_show_alert:true});
            }else{
                that.fuwu_call_bind();
            }
        });
    },
    fuwu_call_bind:function(){
        var that = this;
        if (that.data.this_fuwu_info.f_phone == ''){
            return false;
        }
        wx.showModal({
            title: '提示',
            content: '服务电话：' + that.data.this_fuwu_info.f_phone,
            showCancel:true,
            cancelText:'取消',
            confirmText:'拨打',
            success: function (res) {
                if (res.confirm) {
                    wx.makePhoneCall({phoneNumber: that.data.this_fuwu_info.f_phone});
                } else if (res.cancel) {
                    
                }
            }
        })
    },
    close_alert_bind:function(){
        this.setData({ is_show_alert:false});
    },
    //图片放大
    img_max_bind: function (e) {
        var that = this;
        var img_max_url = e.currentTarget.dataset.url;
        var this_img_key = e.currentTarget.dataset.key;
        var all_img_num = that.data.d_info.shop_xiangce.length;
        var durls = [];
        for (var i = 0; i < all_img_num; i++) {
            durls[i] = that.data.d_info.shop_xiangce[i].img_url;
        }
        wx.previewImage({
            current: img_max_url,
            urls: durls
        })
    },
    onPullDownRefresh: function () {
        var that = this;
        that.get_index_data();
        setTimeout(() => {
            wx.stopPullDownRefresh()
        }, 1000);
    },
    onShareAppMessage: function () {
        var that = this;
        var shareTitle = that.data.d_info.shop_name;
        var shareDesc = '';
        var sharePath = 'pages/app_danye_20180530/index';
        return {
            title: shareTitle,
            desc: shareDesc,
            path: sharePath
        }
    },
    //链接跳转
    go_url_bind: function (e) {
        wx.navigateTo({ url: e.currentTarget.dataset.url });
    },
})