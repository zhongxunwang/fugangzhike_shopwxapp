var _requsetCYZ = require('utils/data');
//####livePlayer####
import util from './utils/util.js';
App({
    onLaunch: function (e) {
        let that = this;
        that.onStartupScene(e.query);

        //####livePlayerJS####

        wx.setStorageSync("login_fail_times", 0);
        //验证用户是否登陆超时
        wx.checkSession({
            success: function (res) {
                //session 未过期，并且在本生命周期一直有效
            },
            fail: function () {
                //登录态过期
                that.getUserDataToken();
                return false;
            }
        })
    },
    /**
     * 生成转发的url参数
     */
    getShareUrlParams(params) {
        let _this = this;
        return util.urlEncode(Object.assign({
            referee_id: _this.getUserId()
        }, params));
    },
    /**
     * 当前用户id
     */
    getUserId() {
        return wx.getStorageSync('user_id');
    },
    /**
     * 获取场景值(scene)
     */
    getSceneData(query) {
        return query.scene ? util.scene_decode(query.scene) : {};
    },
    /**
     * 小程序启动场景
     */
    onStartupScene(query) {
        // 获取场景值
        let scene = query.scene ? util.scene_decode(query.scene) : {};
        // 记录推荐人id
        let refereeId = query.referee_id ? query.referee_id : scene.uid;
        if (refereeId > 0) {
            wx.setStorageSync('referee_id', refereeId);
        }
    },
    checkUserGIsName: function () {
        var that = this;
        wx.request({
            url: _requsetCYZ.caiyuanzi_host_api_url + '/User/checkUserNameIsInfo',
            method: "GET",
            data: {
                utoken: wx.getStorageSync("wuliu_utoken")
            },
            success: function (res) {
                if (res.data.code == 0) {
                    wx.navigateTo({
                        url: "/pages/user/login/index"
                    });
                    return false;
                } else if (res.data.code == 2) {
                    that.getUserDataToken(function (token) {
                        that.checkUserGIsName();
                    });
                }
            }
        });
    },
    getUserDataToken: function (cb) {
        var that = this;
        wx.setStorageSync("wuliu_utoken", '');
        wx.showToast({
            title: '登陆中',
            icon: 'loading',
            duration: 10000,
            mask: true
        });
        var utoken = wx.getStorageSync("wuliu_utoken");
        var fail_times = wx.getStorageSync("login_fail_times");
        if (fail_times > 2) {
            wx.hideToast();
            wx.setStorageSync("login_fail_times", 0);
            return false;
        }
        wx.login({
            success: function (res) {
                var code = res.code;
                wx.request({
                    url: _requsetCYZ.caiyuanzi_host_api_url + '/User/userAuthSlogin',
                    method: "POST",
                    data: {
                        utoken: utoken,
                        code: code,
                        app_token: _requsetCYZ.caiyuanzi_app_token,
                        daili_id: _requsetCYZ.caiyuanzi_daili_id,
                        referee_id: wx.getStorageSync('referee_id'),
                        referee_openid: wx.getStorageSync('global_share_r_openid'),
                        u_lat:wx.getStorageSync('global_u_lat') || 0,
                        u_lng:wx.getStorageSync('global_u_lng') || 0,
                        u_address:wx.getStorageSync('global_u_address') || ''
                    },
                    fail: function (res) {
                        wx.hideToast();
                    },
                    success: function (res) {
                        wx.hideToast();
                        if (res.data.success == -1) {
                            wx.setStorageSync("login_fail_times", fail_times + 1);
                            typeof cb == "function" && cb();
                        } else {
                            wx.setStorageSync("login_fail_times", 0);
                            var utoken = res.data.utoken;
                            wx.setStorageSync("wuliu_utoken", utoken);
                            wx.setStorageSync('user_id', res.data.user_id);
                            typeof cb == "function" && cb(utoken);
                        }
                    }
                })
            },
            fail: function (res) {
                console.log(res);
                wx.hideToast();
            }
        })
    },
    globalData: {
        app_config_data: ''
    },
    pushFormIdSubmit: function (e) {
        let form_id = e.detail.formId;
        wx.request({
            url: _requsetCYZ.caiyuanzi_host_api_url + '/ApiForm/index',
            data: {
                utoken: wx.getStorageSync("wuliu_utoken") || '',
                form_id: form_id,
                app_token: _requsetCYZ.caiyuanzi_app_token,
                daili_id: _requsetCYZ.caiyuanzi_daili_id
            },
            header: {
                'content-type': 'application/json',
            },
            method: 'GET',
            success: function (res) {
                // console.log(res);
            }
        });
    },
    commonErrorTips: function (msg) {
        wx.showToast({
            title: msg,
            icon: 'none',
            duration: 1000
        })
    },
})

/**
 * 注册公用函数
 */
const oldPage = Page;
Page = function (options) {
    options.common_call_phone = function (e) {
        wx.showModal({
            title: '温馨提示',
            content: "联系我时请告知是在小程序【" + e.currentTarget.dataset.title + "】平台看到的",
            success: function (res) {
                if (res.confirm == true) {
                    wx.makePhoneCall({
                        phoneNumber: e.currentTarget.dataset.mobile,
                    });
                }
            }
        })
    },
        options.common_link_url = function (e) {
            const dataset = e.currentTarget.dataset,
                url = dataset.url;
            wx.navigateTo({
                url: url,
                fail: function () {
                    wx.switchTab({
                        url: url
                    });
                }
            });
        },
        options.common_msg_tips = function (msg) {
            wx.showToast({
                title: msg,
                icon: 'none',
                duration: 1000
            })
        },
        options.common_look_img = function (e) {

        },
        oldPage(options);
}
